import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../environments/environment.prod';
//import { environment } from 'src/environments/environment.prod';



@Injectable({
    providedIn: 'root'
})
export class LoginService{
    private forms=[];
    private value=[];
    apiUrl=environment.baseUrl;
   // user=localStorage.getItem('user');
    // setSend(values){
    //     this.value.push(values);
    // }
    // getSend()
    // {
    //     return this.value;
    // }

    constructor(private http: HttpClient,private cookie:CookieService) { }

    login(model){
        let httpheaders = new HttpHeaders();
        httpheaders.append('Content-Type', 'application/json');
        return this.http.post(this.apiUrl+"/Login",model,{ headers : httpheaders })
    }
    getUser(){
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': 'Bearer ' +localStorage.getItem('user'),

              //'Authorization': 'Bearer ' +this.cookie.get('userAuth'),
              
            })
           };

        return this.http.get(this.apiUrl+"/Userdetails",httpOptions)
    }
    forgotPassword(data){
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              //'Authorization': 'Bearer ' +localStorage.getItem('user'),

              //'Authorization': 'Bearer ' +this.cookie.get('userAuth'),
              
            })
           };

        return this.http.put(this.apiUrl+"/Forgotpassword",data,httpOptions)
    } 
    
    logout(){
       // this.router.nav igate(['/addemp']),
        this.cookie.delete('userAuth')
        
    } 
}