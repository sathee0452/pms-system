import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login-service.service';
import { CookieService } from 'ngx-cookie-service';
import { NbToast } from '@nebular/theme';
import { Toaster } from 'ngx-toast-notifications';
//import { AlertService } from '../_helper/alert.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
loginForm:FormGroup
login=[];
  send=false;
  //loginForm: FormGroup;
  loading = false;
  submitted = false;
  display=true;
  localStorage;
  user;
  
  constructor(private router:Router,
    private formBuilder:FormBuilder, 
    private loginservice : LoginService,
    private cookie: CookieService,
    private toaster: Toaster
     ) { }

  ngOnInit() {

    this.loginForm=this.formBuilder.group({
      email:['',Validators.required],
      password:['',Validators.required],

    });
  
}
get fval() { return this.loginForm.controls; }

onFormSubmit() {
  this.submitted = true;
  //this.router.navigate(['/home'])
  
   /////////////////////////////////
    var model = {
      email : this.loginForm.controls.email.value,
      password: this.loginForm.controls.password.value
    }
//------------------------------------------------------------------------
    this.loginservice.login(model).subscribe(result => {
        var res:any = result
         
        if(res.token) {
           
          localStorage.setItem('user',res.token)
         // this.cookie.set("userAuth",res.token)
           //console.log(user)
          this.loginservice.getUser().subscribe(results=>{
            var userDetailsL:any =  results;
            if(userDetailsL.status){
         if(userDetailsL.user.role){
              this.router.navigate(['/pages/dashboard'])
              // this.toaster.open({
              //   text: "Success!! Login",
              //   caption: "Sucess"+ ' notification',
              //   type: "success",
              //   position:"top-right",
      
              // });
            
              }
             }
          })
        }
    },
    catchError=>{
       
      if(catchError.status = 404) {
        this.toaster.open({ 
          text: catchError.error.message,
          caption: "Warning"+ ' notification',
          type: "warning",
          position:"top-right",
          duration:1500
        });
      
      //alert(catchError.error.message)
      // this.alertService.success("cate")
      }
    }
    )

   }
   
   logout(){
    this.cookie.delete('userAuth')
    this.router.navigate(['/']),
    this.display=true;
    }

    forgotPassword(){
      console.log("des")
      this.router.navigate(['/forgotPassword'])
    }


  

}
