import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../login/login-service.service';
import { Toaster } from 'ngx-toast-notifications';

@Component({
  selector: 'ngx-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {
submitted=false;
forgetPassword:FormGroup
  constructor(private formBuilder:FormBuilder,
          private router:Router, 
          private forgotPassword:LoginService,
          private toaster:Toaster
    ) { }

  ngOnInit() {
    this.forgetPassword=this.formBuilder.group({
      email:['',Validators.required],
    });
  }
  get fval() { return this.forgetPassword.controls; }

  sendEmail(){
    this.submitted=true; 
  
    var model={
      email:this.forgetPassword.controls.email.value
    }  
     this.forgotPassword.forgotPassword(model).subscribe(result => {
      var userDetailsL:any =  result;
      this.router.navigate(['/'])

      this.toaster.open({ 
        text: "Email Send ",
        caption: "Success"+ ' notification',
        type: "success",
        position:"top-right",
        duration:1500
      });
          
  },
  catchError=>{
     
    if(catchError.status = 404) {
      this.toaster.open({ 
        text: catchError.error.message,
        caption: "Warning"+ ' notification',
        type: "warning",
        position:"top-right",
        duration:1500
      });
    
    //alert(catchError.error.message)
    // this.alertService.success("cate")
    }
  }
  )

 }

}
