import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../../login/login-service.service';
import { CookieService } from 'ngx-cookie-service';
import { Toaster } from 'ngx-toast-notifications';
import { MustMatch } from '../../../_helper/must-match.validator';
import { EmployeeService } from '../add-employee/employee.service';
import { TicketService } from '../../ticket/ticket.service';

@Component({
  selector: 'ngx-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  loginForm:FormGroup
  login=[];
  id;
    send=false;
    //loginForm: FormGroup;
    loading = false;
    submitted = false;
    display=true;
    localStorage;
    user;
    
    constructor(private router:Router,
      private formBuilder:FormBuilder, 
      private loginService : LoginService,
    //  private ticketService:TicketService,
      private cookie: CookieService,
      private toaster: Toaster,
      private employeeService:EmployeeService
       ) { }
  
    ngOnInit() {
      this.loginService.getUser().subscribe(results=>{
        var userDetailsL:any =  results;
        this.id=userDetailsL.user._id;
        });
          // console.log(this.id)
      this.loginForm=this.formBuilder.group({
        newPassword:['',Validators.required],
        password:['',Validators.required]
      },
      {
        validator: MustMatch('newPassword', 'password')
      })
    
  }
  get fval() { return this.loginForm.controls; }
  
  onFormSubmit() {
    this.submitted = true;
    //this.router.navigate(['/home'])
    if (this.loginForm.invalid) {
      return;
  }
     /////////////////////////////////
      var model = {
        password : this.loginForm.controls.password.value,
      }
  //------------------------------------------------------------------------
  this.employeeService.changePassword(this.id,model).subscribe(res =>{
    var userDetailsL:any =  res;
      //this.router.navigate(['/nav'])
      this.toaster.open({
        text: "Updation Success!! :-"+userDetailsL.name,
        caption: "Success"+ ' notification',
        type: "success",
        position:"top-right",
      });
    
    
     //alert('Update SUCCESS!! :-)\n\n' + userDetailsL.name);
     this.onReset();
     
     
   },
 


catchError=>{
if(catchError.status = 400) {

this.toaster.open({
  text: "Updation Warning!! :-",
  caption: "Warning"+ ' notification',
  type: "warning",
  position:"top-right",
});

}
})
  
}
  onReset(){
    this.submitted = false;
    this.loginForm.reset();
   
    this.router.navigate(['/pages/employee/user-profile'])
  }
    
  
  }
  