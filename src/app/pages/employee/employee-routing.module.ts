import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeComponent } from './employee.component';
import { EmployeeListComponent } from "./employee-list/EmployeeListComponent";
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [{
  path: '',
  component: EmployeeComponent,
  children: [
    {path: 'employee-list',component: EmployeeListComponent} ,
  {path:'add-employee',component:AddEmployeeComponent},
  {path:'user-profile',component:UserProfileComponent},
  {path:'changePassword',component:ChangePasswordComponent}

], 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule { }