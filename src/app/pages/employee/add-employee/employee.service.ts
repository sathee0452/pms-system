import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';  

import {employee} from './employee.model';
 
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../../environments/environment.prod';
 
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  selectedEmployee:employee;
  employees : employee[];
  user=[];
  baseUrl= environment.baseUrl;
  //emplisturl=environment.emplisturl;

  constructor(private Http:HttpClient,
              private cookies:CookieService) { 

  }

  login(model){
    let httpheaders = new HttpHeaders();
    httpheaders.append('Content-Type', 'application/json');
    return this.Http.post(this.baseUrl+"/User",model,{ headers : httpheaders })
}
//-----------------------------------Insert Employee-------------------------------------------------------------

  postEmployee(emp:employee){
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
    };
  
    return this.Http.post(this.baseUrl+"/Employeecreation",emp,httpOptions);
  }
 //-----------------------------------Get current User-------------------------------------------------------------
 getUser(){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/Userdetails",httpOptions)
}

//-----------------------------------get Employee for display-------------------------------------------------------------
getEmployee(details){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Allemployees",details,httpOptions);
}

//-----------------------------------get Employee for display-------------------------------------------------------------
allEmployees(){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.get(this.baseUrl+"/Allusers",httpOptions);
}

//-----------------------------------Update  Employee Email; -------------------------------------------------------------

UpdateEmployee(id,email:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Updateemployees/"+id,email,httpOptions);
}
//-----------------------------------Change  passwors; --d----------------------------------------------------------

changePassword(id,password:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Changepassword/"+id,password,httpOptions);
}


//----------------------------------Search Employee--------------------------------------------------------------
searchEmployee(searchValue:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Searchemployee",searchValue,httpOptions);
}

//-----------------------------------Update  Employee Email; -------------------------------------------------------------

UpdateProfilePhoto(id,url:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Updateprofile/"+id,url,httpOptions);
}
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
}

