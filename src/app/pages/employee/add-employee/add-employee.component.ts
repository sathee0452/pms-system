import { Component, OnInit } from '@angular/core';


//import{EmployeeService} from '../add-employee/employee.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { ValidationService } from '../../_helper/validation.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ValidationService } from '../../../_helper/validation.service';
import { EmployeeService } from './employee.service';
import { LoginService } from '../../../login/login-service.service';
import { Toaster } from 'ngx-toast-notifications';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss'],
  //providers:[EmployeeService]
})
export class AddEmployeeComponent implements OnInit {
// addemp=[];
users=false;
add_employee: FormGroup; 
  submitted = false;
  viewed=false;
  returnUrl: string;
   error = '';

  constructor(private employeeService:EmployeeService,
              private formBuilder:FormBuilder,
              private router:Router,
              private loginservice:LoginService,
              private cookies:CookieService,
              private toaster: Toaster) { }

  ngOnInit() {
    this.add_employee = this.formBuilder.group({
       //id:['',Validators.required],
      title: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['',Validators.required],
      password:['',Validators.required],
      email: ['',Validators.required],
      role:['',Validators.required],
      age:['',Validators.required]
     
    });
    this.employeeService.getUser().subscribe(results=>{
      var userDetailsL:any =  results;
      //alert("Successs"+userDetailsL.user.name+userDetailsL.user.role)
      //if(userDetailsL.user.role==1||userDetailsL.user.role==3)
      if(userDetailsL.user.role!=1){
         this.router.navigate(['/pages/miscellaneous/404'])
       }
      });
  
  }

  get f() { return this.add_employee.controls;}
  
  onSubmit(){
    this.submitted = true;
    // stop here if form is invalid
     
    var emp = {
      // id:this.Add_Employee.controls.id.value,
      name:this.add_employee.controls.firstName.value+" "+this.add_employee.controls.lastName.value,
      email : this.add_employee.controls.email.value,
      role: this.add_employee.controls.role.value,
      password:this.add_employee.controls.password.value
    }

    this.employeeService.postEmployee(emp).subscribe(res =>{
             var userDetailsL:any =  res;
            // alert('SUCCESS!! :-)\n\n' + userDetailsL.name);
            this.toaster.open({
                text: "Success!!"+userDetailsL.name,
                caption: "Success"+ ' notification',
                type: "success",
                position:"top-right",
              });
            
              this.onReset();
              this.router.navigate(['/pages/employee/employee-list'])
            
    },
    catchError=>{
      if(catchError.status = 422) {
        this.toaster.open({ 
          text: catchError.error,
          caption: "Warning"+ ' notification',
          type: "warning",
          position:"top-right"
        });
      
      }
    })
    
  }
  //  
  onReset() {
    this.submitted = false;
    this.add_employee.reset();
    this.router.navigate(['/pages/employee/employee-list'])
}
}
