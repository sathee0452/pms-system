import { Component, OnInit } from '@angular/core';
//import { SmartTableData } from '../../../@core/data/smart-table';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../add-employee/employee.service';
import { CookieService } from 'ngx-cookie-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from '../../../_helper/must-match.validator';
import { Toaster } from 'ngx-toast-notifications';
 
@Component({
  selector: 'ngx-employee',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
serial=0;
  employees=[];
  submitted=false;
  model=[];
  users=false;
  searchName;
  updateEmployee:FormGroup;
  updateuser;
  currentEmail;
  size=10;
  //status:FormGroup
  sino=[];
   pageno: number;
  perPage: number;
  total: number;
 totalPages: number;
 accessPagination=1;
 searchvalue;
 




constructor(private router:Router,
    private employeeService:EmployeeService,
            private cookies:CookieService,
            private formBuilder:FormBuilder,
            private route:ActivatedRoute,
            private toaster:Toaster) { 
              this.pageno=1;
            }



  ngOnInit() { 
    this.getUsuarios(this.pageno);

     
    // ---------------------------------------------------------------
   
    this.updateEmployee=this.formBuilder.group({
      email:['',Validators.required],
      confirmEmail:['',Validators.required]
    }, {
      validator: MustMatch('email', 'confirmEmail')
  });

  
  
  
  
    ////--------------------------get User for disable addemployee---------------------------------------
    this.employeeService.getUser().subscribe(results=>{
      var userDetailsL:any =  results;
      //alert("Successs"+userDetailsL.user.name+userDetailsL.user.role)
      //if(userDetailsL.user.role==1||userDetailsL.user.role==3)
      if(userDetailsL.user.role==1){
         this.users=true;
       }
   
    });
  }

  getUsuarios(pageno1) {
    this.sino=[]
    this.accessPagination=1
      var tick={
       pageno : pageno1,
       size:this.size
       }
     this.employeeService.getEmployee(tick).subscribe((data:any) => {
         this.employees = data.data;
         this.pageno = data.current;
         this.total = data.pages;
         this.totalPages =data.pages;
         });
         for(var i=0;i<this.size;i++){
           var counts=(this.size*(pageno1-1))+(i+1);
           this.sino.push(counts)
         }
          // console.log(this.sino)
      
           }
     mudouPagina(evento) {
       this.getUsuarios(evento.valor); 
       // console.log(evento.valor)
 
     }
     mudouPagina2(evento) {
      this.searchPagination(evento.valor); 
      // console.log(evento.valor)

    }
 
  
  onClick(){
    this.router.navigate(['/pages/employee/add-employee'])
  }


  // ----------------------------------------------------------------------
  onClickPen(id){
    this.currentEmail=id.email;
    this.updateuser=id._id;
    // this.submitted=true;
    }
  // ----------------------------------------------------------------------
  updateEmail(){
    
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateEmployee.invalid) {
      return;
    }
   
       var Emailvalue={
        email:this.updateEmployee.controls.email.value,
          }
    this.employeeService.UpdateEmployee(this.updateuser,Emailvalue).subscribe(res =>{
      var userDetailsL:any =  res;
        //this.router.navigate(['/nav'])
        this.toaster.open({
          text: "Updation Success!! :-"+userDetailsL.name,
          caption: "Success"+ ' notification',
          type: "success",
          position:"top-right",
        });
      
      
       //alert('Update SUCCESS!! :-)\n\n' + userDetailsL.name);
       this.onReset();
       location.reload();
       
     },
   
 

catchError=>{
if(catchError.status = 400) {
  
  this.toaster.open({
    text: "Updation Warning!! :-",
    caption: "Warning"+ ' notification',
    type: "warning",
    position:"top-right",
  });

}
})
    
  }

  get f() { return this.updateEmployee.controls;}
  onReset() {
    this.submitted = false;
    this.updateEmployee.reset();
     
    //if (this.updateEmployee.valid){
      //this.router.navigate(['/pages/employee/employee-list'])
    
    //}
}
reload(){
  this.router.navigate(['/pages/employee/employee-list']) 
}

Search(value) {
  this.accessPagination=2;
  var page=1
  this.searchvalue=value
  this.searchPagination(page)
 }
searchPagination(page){
 this.sino=[]
  var searchvalue={
    name:this.searchvalue,
    pageno:page,
    size:this.size
}
this.employeeService.searchEmployee(searchvalue).subscribe((data: any) => {
  this.employees = data.data;
  this.pageno = data.current;
  this.total = data.pages;
  this.totalPages =data.pages;
  });
  for(var i=0;i<this.size;i++){
    var counts=(this.size*(page-1))+(i+1);
    this.sino.push(counts)
  }
 
}
}
