import { NgModule } from '@angular/core';
import { FormsModule as ngFormsModule } from '@angular/forms';
 import { NbCardModule, NbInputModule, NbTabsetModule, NbRouteTabsetModule, NbStepperModule, NbButtonModule, NbListModule, NbAccordionModule, NbUserModule, NbSelectModule, NbActionsModule, NbIconModule, NbLayoutModule, NbBadgeModule } from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';

import { EmployeeRoutingModule} from './employee-routing.module';
//import { EmployeeComponent } from './employee.component';
import { EmployeeListComponent } from './employee-list/EmployeeListComponent';
import { EmployeeComponent } from './employee.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TicketRoutingModule } from '../ticket/ticket-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ToastNotificationsModule } from "ngx-toast-notifications";
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
// import { NgxPaginationModule } from 'ngx-pagination/dist/ngx-pagination.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { PaginationComponent } from '../employee/pagination/pagination.component';
 
 
const components = [];

@NgModule({
  imports: [
    ThemeModule,
    EmployeeRoutingModule,
     NbCardModule,
    ngFormsModule,
    NbInputModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    NbLayoutModule,
    NbBadgeModule,
     
 
     
     
    //TicketRoutingModule,
    NbSelectModule,
    NbActionsModule,
    NbIconModule,
  //  ToastNotificationsModule

    
  ],
  declarations: [
    EmployeeComponent,
    EmployeeListComponent,
    AddEmployeeComponent,
   UserProfileComponent,
   ChangePasswordComponent,
   PaginationComponent,

    
  ],
})
export class EmployeeModule {}
