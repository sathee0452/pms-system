import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../login/login-service.service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { EmployeeService } from '../add-employee/employee.service';
import { Toaster } from 'ngx-toast-notifications';
import { MustMatch } from '../../../_helper/must-match.validator';
import { ProjectService } from '../../project/project.service';

@Component({
  selector: 'ngx-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  name;
  id;
  updateEmployee:FormGroup;
submitted=false;
currentEmail;
viewChangeEmail=false;
updateuser
base64image;
url;
datum=[];
  
  // base64image = 'https://storage.googleapis.com/pgmt/files/userprofile.jpg'
  // base64image = 'https://storage.googleapis.com/pgmt/files/sample73437.jpg'

  constructor(private loginService:LoginService,
              private router:Router,
              private employeeService:EmployeeService,
              private projectService:ProjectService,
              private toaster:Toaster,
              private formBuilder:FormBuilder) { }
             

  ngOnInit() {
    this.loginService.getUser().subscribe(results=>{
      var userDetailsL:any =  results;
      this.name=userDetailsL.user;
      this.id=userDetailsL.user._id;
    this.base64image =userDetailsL.user.userprofile;
  // console.log(this.base64image)

  this.projectService.Employeesinproject(this.id).subscribe((data:any)=>{
    this.datum=data
  })
 
      });
     
      this.updateEmployee=this.formBuilder.group({
        email:['',Validators.required],
        confirmEmail:['',Validators.required]
      }, {
        validator: MustMatch('email', 'confirmEmail')
    });
  
    }

    changePassword(){
      this.router.navigate(['/pages/employee/changePassword'])
    }

    onClickPen(){
      this.currentEmail=this.name.email;
      // this.submitted=true;
      }
    // ----------------------------------------------------------------------
    updateEmail(){
      
      this.submitted = true;
      // stop here if form is invalid
      if (this.updateEmployee.invalid) {
        return;
      }
     
         var Emailvalue={
          email:this.updateEmployee.controls.email.value,
            }
      this.employeeService.UpdateEmployee(this.id,Emailvalue).subscribe(res =>{
        var userDetailsL:any =  res;
          //this.router.navigate(['/nav'])
          this.toaster.open({
            text: "Updation Success!! :-"+userDetailsL.name,
            caption: "Success"+ ' notification',
            type: "success",
            position:"top-right",
          });
        
        
         //alert('Update SUCCESS!! :-)\n\n' + userDetailsL.name);
         this.onReset();
         this.loginService.getUser().subscribe(results=>{
          var userDetailsL:any =  results;
          this.name=userDetailsL.user;
          this.id=userDetailsL.user._id;
          });
        
         
       },
     
   
  
  catchError=>{
  if(catchError.status = 400) {
       this.toaster.open({
        text: "Updation Warning!! :-"+catchError.error.message,
        caption: "warning"+ ' notification',
        type: "warning",
        position:"top-right",
      });
  }
  })
  
}
    
  
    get f() { return this.updateEmployee.controls;}
    editEmail(){
      this.viewChangeEmail=true;
    }
    onReset() {
      this.submitted = false;
      this.updateEmployee.reset();
      this.viewChangeEmail=false;

      //if (this.updateEmployee.valid){
        //this.router.navigate(['/pages/employee/employee-list'])
      
      //}
  }
  reload(){
    this.router.navigate(['/pages/employee/employee-list']) 
  }

   
onSelectFile(event) {
  if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();

    reader.readAsDataURL(event.target.files[0]); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.url = event.target;
      this.base64image=this.url.result
      // console.log(this.base64image)
      var image={
        image:this.base64image
      }
      this.employeeService.UpdateProfilePhoto(this.id,image).subscribe(data=>{
      var image=data
      })
      // var images={
      //   image:this.imageurl
      // }
    
       
    //   this.ticketService.addImage(this.ticketId,images).subscribe((res) => {
    //     var result= res;
    //     // console.log(result)
    // this.imageObject=[];

    //     this.ticketService.getImage(this.ticketId).subscribe((data:any)=>{
    //       this.imageStrings = data.data;  
    //         //  console.log(this.imageStrings[0].link)
    //         //  console.log(data.count)
      
    //          this. imageCount1=data.count;
    //         //  for(i=0;i<data.count;i++){
    //         //   console.log(i)
              
    //           this.imageStrings.forEach(x => {
    //           //   console.log(x)
    //             var obj = {
    //               image:x.link,
    //               thumbImage:x.link,
                  
    //             }
    //             this.imageObject.push(obj)
    //             // console.log(this.imageObject)
    //           })   
    //     })
       
       
    //   })
      
     
      
    }
  }
  
}
  
}
