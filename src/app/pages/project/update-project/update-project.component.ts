import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from '../project.service';
import { CookieService } from 'ngx-cookie-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Toaster } from 'ngx-toast-notifications';
import { asLiteral } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'ngx-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.scss']
})
export class UpdateProjectComponent implements OnInit {
  valid=true;
  getLead = [];
  getemployee = [];
  emp = [];
  currentProject: any;
  currenttl;
  currentTlId;
  currenttlname;
  currentemp=[];
  currentempdis=[];

  currentupemp=[];
  currentEmpId=[];

  Update_Project:FormGroup;
  projectid: any;
  submitted = false;
  getProjectDes;

  getDescription;
   
  projects = [];
  
  employee;
  selectedItem;
  selectedItems;
  selectedTl;
  nameDes;
  id=[];


  constructor(private router: Router,
    private projectService: ProjectService,
    private cookies: CookieService,
    private formBuilder: FormBuilder,
    private toaster:Toaster) { }


  ngOnInit() {
    this.projectService.getProject()
      .subscribe((data: any) => {
        this.projects = data;
      });
      this.Update_Project = this.formBuilder.group({
        projectName: [''],
        tlead: [''],
        emp1:[''],
        emp: [''],
        description: ['', Validators.required],
        // firstName:['',Validators.required]
      });
  
       
      
      
      this.id =this.projectService.getService();
      this.valid=false;
      this.projectid=this.id[this.id.length-1]._id;
      this.currentProject=this.id[this.id.length-1].projectname;
      this.currenttl=this.id[this.id.length-1].teamlead.name;
      this.currentTlId=this.id[this.id.length-1].teamlead._id;
      this.getDescription=this.id[this.id.length-1].description;
      this.currentempdis=this.id[this.id.length-1].employees;
      this.currentEmpId=this.id[this.id.length-1].employees._id;
      this.currentemp=this.currentempdis;
      
      // console.log(this.id);
      
      //  ----------------get team leader details from Teamlead table----------------------
    this.projectService.getTeamLead()
      .subscribe((data: any) => {
        this.getLead = data;
      });
    //----------------get team members details from Employeelist table----------------------
    this.projectService.getTeamMember()
      .subscribe((data: any) => {
        this.getemployee = data;
      });
}
get f() { return this.Update_Project.controls; }


gettl(){
  this.currentTlId=this.Update_Project.controls.tlead.value;
   }
   
   getemp(){
     this.currentemp=this.Update_Project.controls.emp.value;
   }

   onsubmits(){
  this.submitted = true;
  // stop here if form is invalid
  var data = {
    projectname:(this.Update_Project.controls.projectName.value||this.currentProject),
    teamleader: this.currentTlId,
    employees:this.currentemp,
    description:this.Update_Project.controls.description.value||this.getDescription,
  }
  this.projectService.updateProject(this.projectid,data).subscribe(res => {
     
    this.router.navigate(['/pages/project/project-list'])           
    
    this.onReset();

  },



    catchError => {
      if (catchError.status = 400) {
         
        this.toaster.open({ 
          text: catchError.error,
          caption: "Warning"+ ' notification',
          type: "warning",
          position:"top-right",
          duration:1500
        });
      
      }
    }
  )

}

onReset() {
  this.submitted = false;
  this.Update_Project.reset();
  this.router.navigate(['/pages/project/project-list'])           

}


}