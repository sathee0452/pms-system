import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TicketService } from '../../ticket/ticket.service';
import { CookieService } from 'ngx-cookie-service';
import { Toaster } from 'ngx-toast-notifications';
import { ProjectService } from '../project.service';

@Component({
  selector: 'ngx-project-ticket-details',
  templateUrl: './project-ticket-details.component.html',
  styleUrls: ['./project-ticket-details.component.scss']
})
export class ProjectTicketDetailsComponent implements OnInit {
  status:FormGroup;
  name=[];
  access=1;
  id;
   projectId;
  searchName;
  nameDes;
  getDescription;
  tickets=[];
  openCount=0;
  inprogressCount=0;
  testCount=0;
  closedCount=0;
  size=10;
  //status:FormGroup
  sino=[];
   pageno: number;
  perPage: number;
  total: number;
 totalPages: number;
 paginationAccess=1;
 sortStatus:FormGroup;
 

  // sortStatus:FormGroup;
  // sortPriority:FormGroup;
  
  
   
      constructor(private router:Router,
        private route:ActivatedRoute,
        private formBuilder:FormBuilder,
        private ticketService:TicketService,
        private projectService:ProjectService,
         private toaster:Toaster) { 
           this.pageno=1
         }
    
      ngOnInit() {
        this.sortStatus=this.formBuilder.group({
          sort:['']
        });
      
  
        this.ticketService.setAccess(this.access);
        this.id =this.projectService.getProjectId();
        this.projectId=this.id[this.id.length-1]._id 
        this.getUsuarios(this.pageno);
        
this.projectService.getProjectTicketcount(this.projectId).subscribe((data:any) => {  
  var tickets = data.data;
  // console.log(tickets)
  for(let i of tickets){
    
    if(i.status=="open"){
      this.openCount=this.openCount+1;
    }
    if(i.status=="Inprogress"){
      this.inprogressCount=this.inprogressCount+1;
    }
    if(i.status=="Test"){
      this.testCount=this.testCount+1;
    }
    if(i.status=="closed"){
      this.closedCount=this.closedCount+1;
    }
  }
  })
     //  --------------- u-------------------------------------
      // this.sortStatus=this.formBuilder.group({
      //   sort:['']
      // });
    
  }
  getUsuarios(pageno1) {
 this.paginationAccess=1;
    this.sino=[];
     var tick={
       pageno : pageno1,
       size:this.size
       }
       this.projectService.getProjectTicket(this.projectId,tick).subscribe((data:any) => {  
        this.tickets = data.data; 
        this.pageno = data.current;
       this.total = data.pages;
       this.totalPages =data.pages;
      });
           for(var i=0;i<this.size;i++){
           var counts=(this.size*(pageno1-1))+(i+1);
           this.sino.push(counts)
         }
         // console.log(this.sino)
      
           }
     mudouPagina(evento) {
       this.getUsuarios(evento.valor); 
       // console.log(evento.valor)
 
     }
     mudouPagina1(evento) {
      this.statusPagination(evento.valor); 
      // console.log(evento.valor)

    }
 
  get f() { return this.status.controls; }
  
      
         viewDetails(id){
        this.ticketService.setService(id);
        this.router.navigate(['/pages/ticket/viewTicket'])
      }
      Search(value) {
        var searchvalue={
              name:value
        }
        this.ticketService.searchTicket(searchvalue).subscribe((data: any) => {
          this.tickets = data;
        });
      }
      getDes(tick) {
        this.nameDes=tick.projectname
        this.getDescription = tick.description;
      }
      onChangeStatus(){
        this.paginationAccess=2;
        if(this.sortStatus.controls.sort.value==="all"){
          this.paginationAccess=1;
          this.getUsuarios(this.paginationAccess)
        }
        else{
          var pageStatus =1;
          this.statusPagination(pageStatus)
      
           
        }   
        }
      
        statusPagination(page){
          this.sino=[];
      
          var sort={
            status:this.sortStatus.controls.sort.value,
            pageno:page,
            size:this.size
          }
          // console.log(page)
          this.projectService.sortTicketByProject(this.projectId,sort).subscribe((res:any) => {
            this.tickets=res.data;
            this.pageno = res.current;
            this.total = res.pages;
            this.totalPages =res.pages;
            
            for(var i=0;i<this.size;i++){
              var counts=(this.size*(page-1))+(i+1);
              this.sino.push(counts)
            }
          });
      
        }
      
      
  }
  