import { Component, OnInit } from '@angular/core';


//import{EmployeeService} from '../add-employee/employee.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { ValidationService } from '../../_helper/validation.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
//  import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { ProjectService } from '../project.service';
import { Toaster } from 'ngx-toast-notifications';
//import { ValidationService } from '../../../_helper/validation.service';
//import { EmployeeService } from './employee.service';

 @Component({

  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss'],
  // providers:[MultiSelectModule]
  //providers:[EmployeeService]
})
export class AddProjectComponent implements OnInit {
// addemp=[];
selectedItem;
selectedmultiple;
getLead=[];
  getemployee=[];
  public emp=[];
  Add_Project:FormGroup;
  submitted = false;
  viewed=false;
  returnUrl: string;
   error = '';
   employee;

  
  


   public fields: Object = { text: 'name', value: 'id' };
  public waterMark: string = 'select employee';  
  public box : string = 'Box';
  

  constructor(//private employeeService:EmployeeService,
              private formBuilder:FormBuilder,
              private router:Router,
              private cookies:CookieService,
              private projectService:ProjectService,
              private toaster:Toaster) { }

  ngOnInit() {
    
    this.Add_Project = this.formBuilder.group({
      projectname: ['', Validators.required],
      tl: ['', Validators.required],
      emp: ['', Validators.required],
      description:['',Validators.required],
    // firstName:['',Validators.required]
    
 });
    


//----------------get team leader details from Teamlead table----------------------
this.projectService.getTeamLead()  
.subscribe((data:any) => {  
  this.getLead = data;  
});
//----------------get team members details from Employeelist table----------------------
this.projectService.getTeamMember()  
.subscribe((data:any) => {  
  this.getemployee = data; 
  
//----------------select project team details from Employeelist table---------------
  this.getemployee.forEach(elem=>{
    this.emp.push(
      { name:elem.name,id:elem._id}
      
    )
    
  })
});

 ////--------------------------get User for disable addproject---------------------------------------
 this.projectService.getUser().subscribe(results=>{
  var userDetailsL:any =  results;
  //alert("Successs"+userDetailsL.user.name+userDetailsL.user.role)
  if(userDetailsL.user.role==3){
     this.router.navigate(['/pages/miscellaneous/404'])
   }

});




}

get f() { return this.Add_Project.controls;}



onsubmit(){
this.submitted = true;
// stop here if form is invalid
// console.log(this.Add_Project.controls.tl.value);
var projects = {
// id:this.Add_Employee.controls.id.value,
projectname:this.Add_Project.controls.projectname.value,
teamleader : this.Add_Project.controls.tl.value,
employees: this.Add_Project.controls.emp.value,
description:this.Add_Project.controls.description.value
}

this.projectService.postproject(projects).subscribe(res =>{
var userDetailsL:any =  res;
 //this.router.navigate(['/nav'])
//alert('SUCCESS!! :-)\n\n' + userDetailsL.projectname);

this.onReset();


}
)


catchError=>{
if(catchError.status = 404) {
alert(catchError.error.message)
}
}

}


  onReset() {
    this.router.navigate(['/pages/project/project-list'])           

    this.submitted = false;
    this.Add_Project.reset();
}
}
