import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Toaster } from 'ngx-toast-notifications';

@Component({
  selector: 'ngx-project-ticket',
  templateUrl: './project-ticket.component.html',
  styleUrls: ['./project-ticket.component.scss']
})
export class ProjectTicketComponent implements OnInit {
id;
currentProject: any;
currentProjectId:any;
add_ticket:FormGroup
projectEmp=[];
  submitted=false;
  constructor(private projectService:ProjectService,
              private formBuilder:FormBuilder,
              private toaster:Toaster,
              private router:Router) { }

  ngOnInit() {
    this.id =this.projectService.getService();
    // console.log(this.id)
    this.currentProject=this.id[this.id.length-1].projectname;
    this.projectEmp=this.id[this.id.length-1].employees;
    this.currentProjectId=this.id[this.id.length-1]._id;
    
    this.add_ticket = this.formBuilder.group({
      //id:['',Validators.required],
     // status:['open'],
      tname: ['', Validators.required],
      description: ['', Validators.required],
      //allemp: ['', Validators.required],
      emp: ['', Validators.required],
      typeofissue:['',Validators.required],
      project: ['']
 
    });
 

  }
 get f() { return this.add_ticket.controls; }

  onsubmit() {
    this.submitted = true;
     // stop here if form is invalid
     var ticks = {
       //id:this.add_ticket.controls.id.value,
       name: this.add_ticket.controls.tname.value,
       
       description: this.add_ticket.controls.description.value,
       //createdby: this.add_ticket.controls.allemp.value,
       issuetype: this.add_ticket.controls.typeofissue.value,
       projectname: this.currentProjectId
       //status:this.add_ticket.controls.status.value
     }
  
  
     this.projectService.postTicket(ticks).subscribe((res) => {
       var userDetailsL: any = res;
       //this.router.navigate(['/nav'])
       this.toaster.open({
        text: "Success!! Created "+userDetailsL.projectname,
        caption: "Sucess"+ ' notification',
        type: "success",
        position:"top-right",
        duration:900,
  
      });
  
       //alert('SUCCESS!! :-)');
       //alert(userDetailsL.user.name+userDetailsL.user.role)
       this.onReset();
       this.router.navigate(['/pages/ticket/createdByYou']) 

     },
     
  
  
     catchError => {
       if (catchError.status = 404) {
        this.toaster.open({
          text: "Warning in Ticket Created ",
          caption: "Warning"+ ' notification',
          type: "warning",
          position:"top-right",
          duration:900,
    
        });
    
         
       }
     }
     )
  
   }
    
   onReset() {
     this.submitted = false;
     this.add_ticket.reset();
     this.router.navigate(['/pages/project/project-list'])
  }

}
