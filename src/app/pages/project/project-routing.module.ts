import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectComponent } from './project.component';
 
import { AddProjectComponent } from './add-project/add-project.component';
import { ProjectListComponent } from './Project-list/ProjectListComponent';
import { UpdateProjectComponent } from './update-project/update-project.component';
import { ProjectTicketComponent } from './project-ticket/project-ticket.component';
import { ProjectTicketDetailsComponent } from './project-ticket-details/project-ticket-details.component';

const routes: Routes = [{
  path: '',
  component: ProjectComponent,
  children: [
    {
    path: 'project-list',
    component: ProjectListComponent,
  } ,
  {path:'add-project',component:AddProjectComponent},
  {path:'updateProject',component:UpdateProjectComponent},
  {path:'addProjectTicket',component:ProjectTicketComponent},
  {path:'projectticketDetails',component:ProjectTicketDetailsComponent}

], 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule { }