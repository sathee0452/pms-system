import { NgModule } from '@angular/core';
import { FormsModule as ngFormsModule } from '@angular/forms';
 
import { NbCardModule, 
  NbInputModule,
   NbRouteTabsetModule,
    NbButtonModule, 
    NbListModule, 
    NbSelectModule,
     NbActionsModule, 
     NbIconModule, 
     NbTooltipModule, 
     NbBadgeModule} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
//import { MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';

import { ProjectRoutingModule} from './project-routing.module';
 import {  ProjectComponent } from './project.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectListComponent } from './Project-list/ProjectListComponent';
import { AddProjectComponent } from './add-project/add-project.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { UpdateProjectComponent } from './update-project/update-project.component';
import { ProjectTicketComponent } from './project-ticket/project-ticket.component';
import { ProjectTicketDetailsComponent } from './project-ticket-details/project-ticket-details.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PaginationComponent } from '../project/pagination/pagination.component';
  
  
const components = [];

@NgModule({
  imports: [
    ThemeModule,
    ProjectRoutingModule,
    NbCardModule,
    ngFormsModule,
    NbInputModule,
    ReactiveFormsModule,
    NbRouteTabsetModule,
    NbButtonModule,
    NbListModule,
    NbSelectModule,
    //MultiSelectAllModule,
    NbActionsModule,
    NgxPaginationModule,
    NbIconModule,
    NbTooltipModule,
    Ng2SearchPipeModule,
    NbActionsModule,
    NbBadgeModule
  ],
  declarations: [
    ProjectComponent,
    ProjectListComponent,
    AddProjectComponent,
    UpdateProjectComponent,
    ProjectTicketComponent,
    ProjectTicketDetailsComponent,
    PaginationComponent
  ],
  
     
  
})

export class ProjectModule {}
