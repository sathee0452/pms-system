import { Component } from '@angular/core';

@Component({
  selector: 'ngx-component',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ProjectComponent {
}
