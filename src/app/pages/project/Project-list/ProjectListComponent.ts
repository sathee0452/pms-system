import { Component, OnInit } from '@angular/core';
//import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ProjectService } from '../project.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Toaster } from 'ngx-toast-notifications';
@Component({
  selector: 'ngx-employee',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
  users = false;
  getLead = [];
  getemployee = [];
  emp = [];
  p;
  currentProject: any;
  currenttl;
  currenttlname;
  currentemp=[];
  projectid: any;
  submitted = false;
  getProjectDes;

  getDescription;
  searchName;
  projects = [];
  Update_Project: FormGroup;
  employee;
  selectedItem;
  nameDes;
  form1=[];
  length;

  size=10;
  //status:FormGroup
  sino=[];
   pageno: number;
  perPage: number;
  total: number;
 totalPages: number;
 accessPagination=1;
 searchvalue;
 
  constructor(private router: Router,
                private projectService: ProjectService,
                private cookies: CookieService,
                private formBuilder: FormBuilder,
                private toaster:Toaster) {
                  this.pageno=1;

                }
  ngOnInit() {
    this.getUsuarios(this.pageno);

    // this.projectService.getProject()
    //   .subscribe((data: any) => {
    //     this.projects = data;
    //     this.length=data.length;
       
    //   });

    //----------------get team members details from Employeelist table----------------------
    this.projectService.getTeamMember()
      .subscribe((data: any) => {
        this.getemployee = data;

        //----------------select project team details from Employeelist table---------------
        this.getemployee.forEach(elem => {
          this.emp.push(
            { name: elem.name, id: elem._id }

          )

        })
      });

    ////-------------------------get User for disable addproject---------------------------------------
    this.projectService.getUser().subscribe(results => {
      var userDetailsL: any = results;
      if (userDetailsL.user.role == 1 || userDetailsL.user.role == 2) {
        this.users = true;
      }

    });

  }
 
  getUsuarios(pageno1) {
    this.accessPagination=1
    this.sino=[];
     var tick={
       pageno : pageno1,
       size:this.size
       }
     this.projectService.getProjectPagination(tick).subscribe((data:any) => {
         this.projects = data.data;
         this.pageno = data.current;
         this.total = data.pages;
         this.totalPages =data.pages;
         });
         for(var i=0;i<this.size;i++){
           var counts=(this.size*(pageno1-1))+(i+1);
           this.sino.push(counts)
         }
         // console.log(this.sino)
      
           }
           mudouPagina(evento) {
            this.getUsuarios(evento.valor); 
            // console.log(evento.valor)
      
          }mudouPagina2(evento) {
            this.searchPagination(evento.valor); 
            // console.log(evento.valor)
      
          }
 

  // ----------------------------------------------------------------------
  onClickPen(id) {
     this.projectService.setService(id);
    // this.projectService.setService(id);
    //this.form1 = this.projectService.getService();
    this.router.navigate(['/pages/project/updateProject'])

  }
  addProjectTicket(id){
    this.projectService.setService(id);
    this.router.navigate(['/pages/project/addProjectTicket']);
  }

  getDes(tick) {
    this.nameDes=tick.projectname
    this.getDescription = tick.description;
  }


  onClick() {
    this.router.navigate(['/pages/project/add-project'])
  }
  viewDetails(id){
    this.projectService.setProjectId(id);
    this.router.navigate(['/pages/project/projectticketDetails'])
  }
   
  Search(value) {
    this.accessPagination=2;
    var page=1
    this.searchvalue=value
    this.searchPagination(page)
   }
  searchPagination(page){
    this.sino=[]
    var searchvalue={
      name:this.searchvalue,
      pageno:page,
      size:this.size
  }
  this.projectService.searchProject(searchvalue).subscribe((data: any) => {
    this.projects = data.data;
    this.pageno = data.current;
    this.total = data.pages;
    this.totalPages =data.pages;
    });
    for(var i=0;i<this.size;i++){
      var counts=(this.size*(page-1))+(i+1);
      this.sino.push(counts)
    }
   
  }

  onReset() {
    this.submitted = false;
    this.Update_Project.reset();
  }

}
