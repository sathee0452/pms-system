import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';  

 import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment.prod';
import { project } from './project.model';
import { Ticket } from '../ticket/ticket.model';
 
@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  selectedProject:project;
  projects: project[];
  private forms=[];
  projectId=[];
  valid=false;
  baseUrl=environment.baseUrl;
  //emplisturl=environment.emplisturl;

  constructor(private Http:HttpClient,
              private cookies:CookieService) { 

  }
  setService(formsp){
    this.forms.push(formsp);
}
getService(){
    return this.forms;   
}
setProjectId(id) {
  this.projectId.push(id)
}
getProjectId() {
  return this.projectId;
}


  login(model){
    let httpheaders = new HttpHeaders();
    httpheaders.append('Content-Type', 'application/json');
    return this.Http.post(this.baseUrl+"/User",model,{ headers : httpheaders })
}
// -----------------insert project------------------------------------------
  postproject(projects:project){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
 
    return this.Http.post(this.baseUrl+"/Projectcreation",projects,httpOptions);
  }
  //-----------------------------------Update  project ; -------------------------------------------------------------

updateProject(id,data:project){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Projectupdate/"+id,data,httpOptions);
}

  // ---------------------get team Leader--------------------------------------
  getTeamLead(){
    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer ' +localStorage.getItem('user'),
        })
       };
    return this.Http.get(this.baseUrl+"/TeamLeadlist",httpOptions)
}
//-----------------------------------Get current User-------------------------------------------------------------
getUser(){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/Userdetails",httpOptions)
}

// ----------------------get Team Member-------------------------------------
getTeamMember(){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/Employeelist",httpOptions)
}
// --------------------get Project pagination---------------------------------------
getProjectPagination(details){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Projectdetailspagination",details,httpOptions);
}
// --------------------get Project pagination---------------------------------------
getProject(){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.get(this.baseUrl+"/Projectdetails",httpOptions);
}
Employeesinproject(id){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.get(this.baseUrl+"/Employeesinproject/"+id,httpOptions);
}

// -------------------------Add Ticket----------------------------------
postTicket(ticks:Ticket){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Ticketcreation",ticks,httpOptions);
}
getProjectTicketcount(ids){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
     'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Projectsort/"+ids,httpOptions);
}
sortTicketByProject(ids,data){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
     'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Sortticketonproject/"+ids,data,httpOptions);
}


getProjectTicket(id,pagination){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
       'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Projectsortpagination/"+id,pagination,httpOptions);
}


// ------------------------search project---------------------------------

searchProject(searchValue:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Searchproject",searchValue,httpOptions);
}



}


