import { Component } from '@angular/core';
//import { ProfitBarAnimationChartData } from '../../../../@core/data/profit-bar-animation-chart';
import { takeWhile } from 'rxjs/operators';
import { LoginService } from '../../../../login/login-service.service';

@Component({
  selector: 'ngx-stats-card-front',
  styleUrls: ['./stats-card-front.component.scss'],
  templateUrl: './stats-card-front.component.html',
})
export class StatsCardFrontComponent {
  

display=[];
role:any=[];
email=[];
  private alive = true;

  linesData: { firstLine: number[]; secondLine: number[] };

  constructor(
              private loginService:LoginService) {
  }
  ngOnInit() {
     
    this.loginService.getUser().subscribe(results=>{
        var userDetailsL:any =  results;
        if(userDetailsL.user.role){
           this.display=userDetailsL.user.name;
           this.role=userDetailsL.user.role;
           this.email=userDetailsL.user.email;

        }
      });
}
}
