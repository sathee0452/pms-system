import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../login/login-service.service';
import { dashboardService } from '../../e-commerce/dashboard-service.service';
import { TicketService } from '../../ticket/ticket.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-duedate',
  templateUrl: './duedate.component.html',
  styleUrls: ['./duedate.component.scss']
})
export class DuedateComponent implements OnInit {
details=[];
count=0;
access=2;
  constructor(private dashboardService:dashboardService,
              private TicketService:TicketService,
              private router:Router,
              private ticketService:TicketService) { }

  ngOnInit() {
    this.ticketService.setAccess(this.access);

this.dashboardService.dueDate().subscribe((data:any)=>{
   this.details=data;
   for(let i of data){
    
    if(i.status!="closed"){
      this.count=this.count+1;
    
          }
      }
  })
  
  }

  getTicketDetails(tick){
    this.TicketService.setService(tick);
  this.router.navigate(['/pages/ticket/viewTicket'])
  }

}
