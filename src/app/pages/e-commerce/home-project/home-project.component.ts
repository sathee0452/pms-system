import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ProjectService } from '../../project/project.service';
import { CookieService } from 'ngx-cookie-service';
import { Toaster } from 'ngx-toast-notifications';

@Component({
  selector: 'ngx-home-project',
  templateUrl: './home-project.component.html',
  styleUrls: ['./home-project.component.scss']
})
export class HomeProjectComponent implements OnInit {
  users = false;
  getLead = [];
  getemployee = [];
  emp = [];
  currentProject: any;
  currenttl;
  currenttlname;
  currentemp=[];
  projectid: any;
  submitted = false;
  getProjectDes;

  getDescription;
  searchName;
  projects = [];
  Update_Project: FormGroup;
  employee;
  selectedItem;
  nameDes;
  form1=[];

 



  constructor(private router: Router,
    private projectService: ProjectService,
    private cookies: CookieService,
    private formBuilder: FormBuilder,
    private toaster:Toaster) { }




  ngOnInit() {
    this.projectService.getProject()
      .subscribe((data: any) => {
        this.projects = data;
      });

  


    //----------------get team members details from Employeelist table----------------------
    this.projectService.getTeamMember()
      .subscribe((data: any) => {
        this.getemployee = data;

        //----------------select project team details from Employeelist table---------------
        this.getemployee.forEach(elem => {
          this.emp.push(
            { name: elem.name, id: elem._id }

          )

        })
      });

    ////-------------------------get User for disable addproject---------------------------------------
    this.projectService.getUser().subscribe(results => {
      var userDetailsL: any = results;
      if (userDetailsL.user.role == 1 || userDetailsL.user.role == 2) {
        this.users = true;
      }

    });

  }
 


  // ----------------------------------------------------------------------
  onClickPen(id) {
     this.projectService.setService(id);
    // this.projectService.setService(id);
    //this.form1 = this.projectService.getService();
    this.router.navigate(['/pages/project/updateProject'])

  }

  getDes(tick) {
    this.nameDes=tick.projectname
    this.getDescription = tick.description;
  }


  onClick() {
    this.router.navigate(['/pages/project/add-project'])
  }

  onReset() {
    this.submitted = false;
    this.Update_Project.reset();
  }

}
