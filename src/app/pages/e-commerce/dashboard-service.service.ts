import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment.prod';
  


@Injectable({
    providedIn: 'root'
})
export class dashboardService{
    private forms=[];
    private value=[];
    apiUrl=environment.baseUrl;
   // user=localStorage.getItem('user');
    // setSend(values){
    //     this.value.push(values);
    // }
    // getSend()
    // {
    //     return this.value;
    // }

    constructor(private http: HttpClient,private cookie:CookieService) { }

    dueDate(){
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'Bearer ' +localStorage.getItem('user')
          })
        };
    //   console.log(localStorage.getItem('user'))
        return this.http.get(this.apiUrl+"/Duedate",httpOptions);
      }
    
    
      
}