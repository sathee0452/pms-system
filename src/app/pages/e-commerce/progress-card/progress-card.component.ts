import { Component, OnInit } from '@angular/core';
import { TicketService } from '../../ticket/ticket.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-progress-card',
  templateUrl: './progress-card.component.html',
  styleUrls: ['./progress-card.component.scss']
})
export class ProgressCardComponent implements OnInit {
  count=0;
  incount=0;
  assignforyou=[];
  next;
  access=2;
  constructor(private ticketService:TicketService,
              private router:Router
    ) { }

  ngOnInit() { 
    this.ticketService.setAccess(this.access);

        //  ---------------get  all ticket created by you-------------------------------------
  this.ticketService.getTicketForYou()  
  .subscribe((data:any) => {  
    this.assignforyou = data;
    for(let i of data){
    
    if(i.status=="open"){
      this.count=this.count+1;
    }
    if(i.status=="Inprogress"){
      this.incount=this.incount+1;
    }
  }
  });

  }
  
  getTicketDetails(tick){
    this.ticketService.setService(tick);
  this.router.navigate(['/pages/ticket/viewTicket'])
  }

}
