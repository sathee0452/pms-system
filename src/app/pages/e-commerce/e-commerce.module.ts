import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
  NbProgressBarModule,
  NbTabsetModule,
  NbUserModule,
  NbIconModule,
  NbSelectModule,
  NbListModule,
  NbActionsModule,
  NbTooltipModule,
  NbLayoutModule,
  NbBadgeModule,
} from '@nebular/theme';
 
import { ThemeModule } from '../../@theme/theme.module';
import { ECommerceComponent } from './e-commerce.component';
import { ProfitCardComponent } from './profit-card/profit-card.component';
//  import { ChartModule } from 'angular2-chartjs';

 
 

import { StatsCardFrontComponent } from './profit-card/front-side/stats-card-front.component';
import { HomeProjectComponent } from './home-project/home-project.component';
import { ProgressCardComponent } from './progress-card/progress-card.component';
import { DuedateComponent } from './duedate/duedate.component';
  
@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbIconModule,
    NbTabsetModule,
    NbSelectModule,
    NbListModule,
    NbActionsModule,
    NbTooltipModule, 
    NbLayoutModule,
    NbBadgeModule,


  ],
  declarations: [
    ECommerceComponent,
    StatsCardFrontComponent,
    ProfitCardComponent,
      
     HomeProjectComponent,
     ProgressCardComponent,
     DuedateComponent
     ],
  providers: [
    
  ],
})
export class ECommerceModule { }
