import { Component } from '@angular/core';
import { MENU_ITEMS } from './pages-menu';
import { LoginService } from '../login/login-service.service';
import { MENU_ITEMS1 } from './pages-menu1';
import { MENU_ITEMS2 } from './pages-menu2';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {
  menu;
  constructor(private loginservice: LoginService,
  ) { }
  ngOnInit() {
    this.loginservice.getUser().subscribe(results => {
      var userDetailsL: any = results;
      if (userDetailsL.status) {
        if (userDetailsL.user.role == 1) {
          this.menu = MENU_ITEMS;
        }
        if (userDetailsL.user.role == 2) {
          this.menu = MENU_ITEMS1;
        }
        if (userDetailsL.user.role == 3) {
          this.menu = MENU_ITEMS2;
        }
      }
    })
    catchError => {
      if (catchError.status = 404) {
        alert(catchError.error.message)
      }
    }
  }
}