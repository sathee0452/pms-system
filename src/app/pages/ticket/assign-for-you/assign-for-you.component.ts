import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { Toaster } from 'ngx-toast-notifications';

@Component({
  selector: 'ngx-assign-for-you',
  templateUrl: './assign-for-you.component.html',
  styleUrls: ['./assign-for-you.component.scss']
})
export class AssignForYouComponent implements OnInit {
status:FormGroup;
paginationAccess=1;
name=[];
access=2;
searchName;
ticketUpdateId;
getDescription;
tickets=[];
sortStatus:FormGroup;
sortPriority:FormGroup;
sino=[];
size=10;

//status:FormGroup
 pageno: number;
perPage: number;
total: number;
totalPages: number;



 
    constructor(private router:Router,
      private route:ActivatedRoute,
      private formBuilder:FormBuilder,
      private ticketService:TicketService,
      private toaster:Toaster) {
        this.pageno=1;
       }
  
    ngOnInit() {
      this.getUsuarios(this.pageno);

      this.ticketService.setAccess(this.access);

      this.status= this.formBuilder.group({
        update:['']
      });
      this.sortPriority=this.formBuilder.group({
        changePriority:['']
      })
      
   //  ---------------Assigned for You-------------------------------------
     this.sortStatus=this.formBuilder.group({
      sort:['']
    });
  

}
getUsuarios(pageno1) {
  this.sino=[];
   var tick={
     pageno : pageno1,
     size:10
     }
     
   this.ticketService.Assignedforyoupagination(tick).subscribe((data:any) => {
       this.tickets = data.data;
       this.pageno = data.current;
       this.total = data.pages;
       this.totalPages =data.pages;
       });
       for(var i=0;i<this.size;i++){
         var counts=(this.size*(pageno1-1))+(i+1);
         this.sino.push(counts)
       }
       // console.log(this.sino)
    
         }
   mudouPagina(evento) {
     this.getUsuarios(evento.valor); 
     // console.log(evento.valor)

   }
   mudouPagina1(evento) {
    this.statusPagination(evento.valor); 
    // console.log(evento.valor)
  }
  mudouPagina3(evento) {
    this.prioritypagination(evento.valor); 
    // console.log(evento.valor)
  }
  
  


get f() { return this.status.controls; }

    
    get fval() { return this.sortStatus.controls;}
    get getPriority(){return this.sortPriority.controls}
   // ------------------------------
   Search(value) {
    var searchvalue={
          name:value
    }
    this.ticketService.searchTicket(searchvalue).subscribe((data: any) => {
      this.tickets = data;
    });
  }
  
   // ------------------------------

   getDes(tick){
    this.getDescription=tick.description;
  }
onChangeStatus(){
  this.paginationAccess=2;
  if(this.sortStatus.controls.sort.value==="all"){
    this.paginationAccess=1;
    this.sino=[];
   var tick={
     pageno : this.pageno,
     size:10
     }
     
   this.ticketService.Assignedforyoupagination(tick).subscribe((data:any) => {
       this.tickets = data.data;
       this.pageno = data.current;
       this.total = data.pages;
       this.totalPages =data.pages;
       });
       for(var i=0;i<this.size;i++){
         var counts=(this.size*(this.pageno-1))+(i+1);
         this.sino.push(counts)
       }
 
  }
  else{
    var pageStatus =1;
    this.statusPagination(pageStatus)

     
  }   
  }

  statusPagination(page){
    this.sino=[];

    var sort={
      status:this.sortStatus.controls.sort.value,
      pageno:page,
      size:this.size
    }
    // console.log(page)
    this.ticketService.assignStatusSort(sort).subscribe((res:any) => {
      this.tickets=res.data;
      this.pageno = res.current;
      this.total = res.pages;
      this.totalPages =res.pages;
      
      for(var i=0;i<this.size;i++){
        var counts=(this.size*(page-1))+(i+1);
        this.sino.push(counts)
      }
    });

  }
    viewDetails(id){
      this.ticketService.setService(id);
      this.router.navigate(['/pages/ticket/viewTicket'])
    }
    
    SortPriority(){
      this.paginationAccess=3;

      if(this.sortPriority.controls.changePriority.value=="5"){
        this.paginationAccess=1;
        this.sino=[];
   var tick={
     pageno : this.pageno,
     size:10
     }
     
   this.ticketService.Assignedforyoupagination(tick).subscribe((data:any) => {
       this.tickets = data.data;
       this.pageno = data.current;
       this.total = data.pages;
       this.totalPages =data.pages;
       });
       for(var i=0;i<this.size;i++){
         var counts=(this.size*(this.pageno-1))+(i+1);
         this.sino.push(counts)
       }
 

      }
      else{
        var priorityPage=1
        this.prioritypagination(priorityPage)
        }
        
  } 
    prioritypagination(page){
      this.sino=[];
      var sort={
        priority:this.sortPriority.controls.changePriority.value,
        pageno:page,
        size:this.size
      }
      this.ticketService.sortPriority(sort).subscribe((res:any) => {
        this.tickets=res.data;
        this.pageno = res.current;
        this.total = res.pages;
        this.totalPages =res.pages;
        });
        for(var i=0;i<this.size;i++){
          var counts=(this.size*(page-1))+(i+1);
          this.sino.push(counts)
        }
    }
}
