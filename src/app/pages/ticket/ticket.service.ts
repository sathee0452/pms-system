import { Injectable } from '@angular/core';
//import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Ticket } from './ticket.model';
import { environment } from '../../../environments/environment.prod';
import { statusChange, id } from './stautsChange.model';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  tickets:Ticket[];
  createdbyyou:Ticket[];
  closedTicket=[];
  _id:any;
  projid:any
  forms=[];
  access=[];


  baseUrl= environment.baseUrl;
  constructor(private Http:HttpClient,
               private cookies:CookieService) { }
               
  setService(formsp) {
    this.forms.push(formsp);
  }
  getService() {
    return this.forms;

  }
  setAccess(acces) {
    this.access.push(acces)
  }
  getAccess() {
    return this.access;

  }


login(model){
  let httpheaders = new HttpHeaders();
  httpheaders.append('Content-Type', 'application/json');
  return this.Http.post(this.baseUrl+"/Login",model,{ headers : httpheaders })
}

// ---------------------update Ticket Status close-------------------------------------
 changeStatus(_id:any,ticks:statusChange){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Ticketclose/"+_id,ticks,httpOptions);
}


// ---------------------update Ticket Status  in progress/test-------------------------------------
UpdateStatus(_id:any,update:statusChange){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Ticketstatusupdate/"+_id,update,httpOptions);
}

 // ---------------------get all  employeeteam-------------------------------------
 getalldata(){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/AllUsers",httpOptions)
}
// ---------------------get ticket details By yoU-------------------------------------
getTicketByYou(){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/Byyou",httpOptions)
}
getTicketByYoupagination(byyou){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.post(this.baseUrl+"/Byyoupagination",byyou,httpOptions)
}
// ---------------------get ticket details Assigned for you-------------------------------------
getTicketForYou(){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/Assignedforyou",httpOptions)
}
Assignedforyoupagination(foryoupage){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.post(this.baseUrl+"/Assignedforyoupagination",foryoupage,httpOptions)
}


// ----------------------get Team Member-------------------------------------
getTeamMember(){
const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    }) 
   };
return this.Http.get(this.baseUrl+"/Allusers",httpOptions)
}
// --------------------get Project---------------------------------------
getProject(){
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer ' +localStorage.getItem('user'),
  })
};

return this.Http.get(this.baseUrl+"/Projectdetails",httpOptions);
}
// ------------------------Get Ticket for Display ----------------------------------
getTicket(page){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Ticketdetails",page,httpOptions);
}



// ------------------------post Ticket insert ticket---------------------------------

postTicket(ticks:Ticket){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Ticketcreation",ticks,httpOptions);
}

// ------------------------search ticket---------------------------------
searchTicket(searchValue:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Searchticket",searchValue,httpOptions);
}

Searchticketbyyou(searchValue:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Searchticketbyyou",searchValue,httpOptions);
}
Searchticketforyou(searchValue:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Searchticketforyou",searchValue,httpOptions);
}

// ------------------------Get project  Employee ----------------------------------
projectEmployeeList(id){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.get(this.baseUrl+"/Projectemployeelist/"+id,httpOptions);
}
// ------------------------Get Status wise sort Searchticketforyou    ----------------------------------
StatusSort(status:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/sort",status,httpOptions);
}
Sortstatusbyyou(status:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Sortstatusbyyou",status,httpOptions);
}
assignStatusSort(status){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Foryou",status,httpOptions);
}

//// ------------------------//// ------------------------
addComment(id,comment:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Commentcreation/"+id,comment,httpOptions);
}
//// ------------------------//// ------------------------


getComment(id){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/Getcomment/"+id,httpOptions)
}
//// ------------------------//// ------------------------
updateComment(id,comment:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Updatecomment/"+id,comment,httpOptions);
}

//// ------------------------//// ------------------------
deleteComment(id){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.delete(this.baseUrl+"/DeleteComment/"+id,httpOptions);
}

//// ------------------------//// ------------------------
updateDuedate(id,duedate){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Updateduedate/"+id,duedate,httpOptions);
}

//// ------------------------//// ------------------------
updatePriority(id,priority){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Updatepriority/"+id,priority,httpOptions);
}

//// ------------------------//// ------------------------
 sortPriority(priority:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Prioritysort",priority,httpOptions);
}
//// ------------------------//// ------------------------

updateEmployee(id,employee){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Ticketupdate/"+id,employee,httpOptions);
}
//// ------------------------//// ------------------------
SortEmployee(employe){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Employeesearch",employe,httpOptions);
}
//// ------------------------//// ------------------------

addImage(id,url:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.post(this.baseUrl+"/Addimage/"+id,url,httpOptions);
}

//// ------------------------//// ------------------------


getImage(id){
  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' +localStorage.getItem('user'),
      })
     };
  return this.Http.get(this.baseUrl+"/Getimage/"+id,httpOptions)
}
 
//// ------------------------//// ------------------------
deleteImage(id){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.delete(this.baseUrl+"/Deleteimage/"+id,httpOptions);
}
/// ------------------------//// ------------------------
updateDescription(id,des:any){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.put(this.baseUrl+"/Updatedescription/"+id,des,httpOptions);
}

//// ------------------------//// ------------------------
// ------------------------Get Ticket for Display by id ----------------------------------
Ticketdetailsbyid(id){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' +localStorage.getItem('user'),
    })
  };

  return this.Http.get(this.baseUrl+"/Ticketdetailsbyid/"+id,httpOptions);
}

}