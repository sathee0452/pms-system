import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { CookieService } from 'ngx-cookie-service';
import { id } from '../stautsChange.model';
import { Router } from '@angular/router';
import { Toaster } from 'ngx-toast-notifications';

@Component({
  selector: 'ngx-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.scss']
})
export class AddTicketComponent implements OnInit {
  getemps = [];
  getemployee = [];
  getproject = [];
  //public emp = [];
  add_ticket: FormGroup;
  submitted = false;
  viewed = false;
  returnUrl: string;
  error = '';
  projemp=[];
  projid;
  not;
  
  constructor(private ticketService: TicketService,
    private router:Router,
    private cookies: CookieService,
    private formBuilder: FormBuilder,
    private toaster:Toaster) { }

  ngOnInit() {
     //----------------get   Teamlead table----------------------
     this.ticketService.getalldata()
     .subscribe((data: any) => {
       this.getemps = data;
     });
   //----------------get project----------------------
   this.ticketService.getProject().subscribe((data: any) => {
       this.getproject = data;
       
     });
      //--------------------------------------
    
 
//  this.ticketService.getProject().subscribe((data: any) => {
//   //this.getproject = data;
//   var userDetailsL:any =  data;
//   if(userDetailsL._id==this.add_ticket.controls.project.value){
//      this.projemp=userDetailsL.employees;
//   }

// });




   this.add_ticket = this.formBuilder.group({
     //id:['',Validators.required],
    // status:['open'],
     tname: ['', Validators.required],
     description: ['', Validators.required],
     //allemp: ['', Validators.required],
     emp: ['', Validators.required],
     project: ['', Validators.required],
     typeofissue:['',Validators.required]

   });

  
 //----------------get project----------------------


 }
 get f() { return this.add_ticket.controls; }
   
 
 
 onsubmit() {
  this.submitted = true;
   // stop here if form is invalid
   var ticks = {
     //id:this.add_ticket.controls.id.value,
     name: this.add_ticket.controls.tname.value,
     description: this.add_ticket.controls.description.value,
     //createdby: this.add_ticket.controls.allemp.value,
     issuetype: this.add_ticket.controls.typeofissue.value,
     projectname: this.add_ticket.controls.project.value,
     
     //status:this.add_ticket.controls.status.value
   }


   this.ticketService.postTicket(ticks).subscribe((res) => {
     var userDetailsL: any = res;
     //this.router.navigate(['/nav'])
     this.toaster.open({
      text: "Success!! Created "+userDetailsL.projectname,
      caption: "Sucess"+ ' notification',
      type: "success",
      position:"top-right",
      duration:900,

    });

     //alert('SUCCESS!! :-)');
     //alert(userDetailsL.user.name+userDetailsL.user.role)
     this.onReset();
     this.router.navigate(['/pages/ticket/createdByYou']) 
   }
   
   )

   catchError => {
     if (catchError.status = 404) {
      this.toaster.open({
        text: "warning!! Creation ",
        caption: "Warning"+ ' notification',
        type: "warning",
        position:"top-right",
        duration:900,
  
      });
  
     }
   }
   

 }
  
 onReset() {
   this.submitted = false;
   this.add_ticket.reset();
   this.router.navigate(['/pages/ticket/listTicket']) 
}
projectEmployeeList(){
  
     this.projid=this.add_ticket.controls.project.value
    //----------------get team members details from Employeelist table----------------------
 this.ticketService.projectEmployeeList(this.projid).subscribe((data: any) => {
  this.getemployee = data.employees;
  this.not=data._id
  
});

 }
  

} 


