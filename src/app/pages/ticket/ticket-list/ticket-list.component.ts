import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TicketService } from '../ticket.service';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { EmployeeService } from '../../employee/add-employee/employee.service';
// import { FetchdataService } from './fetchdata.service';


@Component({
  selector: 'ngx-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {
  tickets = [];
  accessPagination=1;
   getDescription;
  searchName;
   totalPages;
  sino = [];
  //pages = { size: this.size, pageno: this.pageno };
  config: any;
  collection = [];
  currentPage;
  itemsPerPage;
  access=1;
  allemp;
  sortStatus:FormGroup;
  EmployeeSort:FormGroup;
 size=10;
  //status:FormGroup
   pageno: number;
  perPage: number;
  total: number;
  searchvalue;
  // totalPages: number;

   
   

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private employeeService:EmployeeService,
    private ticketService: TicketService,
    private cookies: CookieService,
    private route:ActivatedRoute,
    // private fetchData: FetchdataService,
  ) {
    this.pageno=1;
  }

  ngOnInit() {
    //  ---------------get  all ticket-------------------------------------

    this.getUsuarios(this.pageno);
    //  ---------------get  all ticket-------------------------------------

    this.sortStatus=this.formBuilder.group({
      sort:['']
    });
    this.EmployeeSort=this.formBuilder.group({
      emp:['']
    })
    this.employeeService.allEmployees().subscribe(employee=>{
       this.allemp=employee;
    })  
       this.ticketService.setAccess(this.access);

    

  }
  getUsuarios(pageno1) {
   this.sino=[];
    var tick={
      pageno : pageno1,
      size:10
      }
      
    this.ticketService.getTicket(tick).subscribe((data:any) => {
        this.tickets = data.data;
        this.pageno = data.current;
        this.total = data.pages;
        this.totalPages =data.pages;
        });
        for(var i=0;i<this.size;i++){
          var counts=(this.size*(pageno1-1))+(i+1);
          this.sino.push(counts)
        }
        // console.log(this.sino)
     
          }
    mudouPagina(evento) {
      this.getUsuarios(evento.valor); 
    }
    mudouPagina2(evento) {
      this.employeeTicketpagination(evento.valor); 
    }
    mudouPagina3(evento) {
      this.statusPagination(evento.valor); 
    }
    mudouPagina4(evento) {
      this.searchPagination(evento.valor); 
    }


 get f() { return this.EmployeeSort.controls; }

   
get fval() { return this.sortStatus.controls;}

  onClick() {
    this.router.navigate(['/pages/ticket/addticket']);//add Ticket
  }

  getDes(tick) {
    this.getDescription = tick.description;//.. Ticket description in  modal
  }
//  ---------------------------------------------------------------------------

Search(value) {
  this.accessPagination=4;
  var page=1
  this.searchvalue=value
  this.searchPagination(page)
}
searchPagination(page){
  var searchvalue={
    name:this.searchvalue,
    pageno:page,
    size:this.size
}
this.ticketService.searchTicket(searchvalue).subscribe((data: any) => {
  this.tickets = data.data;
  this.pageno = data.current;
  this.total = data.pages;
  this.totalPages =data.pages;
  });
  for(var i=0;i<this.size;i++){
    var counts=(this.size*(page-1))+(i+1);
    this.sino.push(counts)
  }
 
}
   
  //  ---------------------------------------------------------------------------
  
  
viewDetails(id){
  this.ticketService.setService(id);
  this.router.navigate(['/pages/ticket/viewTicket'])
}
onChangeStatus(){
  this.accessPagination=3;
  this.tickets=[]
  if(this.sortStatus.controls.sort.value=="all"){
    this.accessPagination=1
    this.getUsuarios(this.accessPagination)
  }
  else{
    var page=1;
    this.statusPagination(page)
  }
      
}

statusPagination(page){
   this.sino=[]
  var sort={
    status:this.sortStatus.controls.sort.value,
    pageno:page,
    size:this.size
  }
  this.ticketService.StatusSort(sort).subscribe((res:any) => {
    this.tickets = res.data;
    this.pageno = res.current;
    this.total = res.pages;
    this.totalPages =res.pages;
    });
    for(var i=0;i<this.size;i++){
      var counts=(this.size*(page-1))+(i+1);
      this.sino.push(counts)
    }
      
}
sortEmployee(){
  this.accessPagination=2;
  if(this.EmployeeSort.controls.emp.value=="all"){
    this.accessPagination=1;

//  ---------------get  all ticket-------------------------------------
this.sino=[];
    var tick={
      pageno : this.pageno,
      size:10
      }
      
    this.ticketService.getTicket(tick).subscribe((data:any) => {
        this.tickets = data.data;
        this.pageno = data.current;
        this.total = data.pages;
        this.totalPages =data.pages;
        });
        for(var i=0;i<this.size;i++){
          var counts=(this.size*(this.pageno-1))+(i+1);
          this.sino.push(counts)
        }
        // console.log(this.sino)
     
          
   
}
else{
   var page=1;
   this.employeeTicketpagination(page)
}
}
employeeTicketpagination(page){
   this.sino=[]
   var Employeeid=this.EmployeeSort.controls.emp.value;
   var employeeId={
     id:Employeeid,
     pageno:page,
     size:this.size
   }
   this.ticketService.SortEmployee(employeeId).subscribe((data:any)=>{
    this.tickets = data.data;
    this.pageno = data.current;
        this.total = data.pages;
        this.totalPages =data.pages;
        });
        for(var i=0;i<this.size;i++){
          var counts=(this.size*(page-1))+(i+1);
          this.sino.push(counts)
        }
        
}
}
