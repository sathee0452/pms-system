import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketComponent } from './ticket.component';
 //import { EmployeeComponent } from '../employee/employee-list/employee.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';
import { CreatedByYouComponent } from './created-by-you/created-by-you.component';
import { AssignForYouComponent } from './assign-for-you/assign-for-you.component';
import { ClosedTicketComponent } from './closed-ticket/closed-ticket.component';
import { UpdateProjectComponent } from '../project/update-project/update-project.component';
import { ViewTicketComponent } from './view-ticket/view-ticket.component';

const routes: Routes = [{
  path: '',
  component: TicketComponent,
  children: [
      {
      path: 'listTicket',component:TicketListComponent},
    {path: 'addticket',component:AddTicketComponent}, 
    {path: 'createdByYou',component:CreatedByYouComponent}, 
    {path: 'assignedForYou',component:AssignForYouComponent}, 
    {path: 'closedTicket',component:ClosedTicketComponent},
    {path:'viewTicket',component:ViewTicketComponent}
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketRoutingModule {
}
