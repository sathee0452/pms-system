import { Component, OnInit } from '@angular/core';
import { TicketService } from '../../ticket.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Toaster } from 'ngx-toast-notifications';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-ticket-comment',
  templateUrl: './ticket-comment.component.html',
  styleUrls: ['./ticket-comment.component.scss']
})
export class TicketCommentComponent implements OnInit {
  submitted=false;
  ticketId;
  addComment:FormGroup
  getComment;
commentStatus=false;
 editComment=false;
 comment;
 uid;
 id;
 
  constructor(private ticketService:TicketService,
    private fb: FormBuilder,
    private toaster:Toaster,
    private router:Router
    ) { }

  ngOnInit() {
    this.id =this.ticketService.getService();
        this.ticketId=this.id[this.id.length-1]._id
        
    this.addComment = this.fb.group({
      comment: ['', Validators.required],
    });
    
    this.ticketService.getComment(this.ticketId).subscribe(data=>{
      this.getComment = data;  
       
    })

  }
  // ---------------------------------------Comment----------------------------------------------------------------------------------
  onComment(){
    this.commentStatus=true;

  }
  onsubmitComment(){
    this.submitted=true;
    if (this.addComment.invalid) {
          return;
      }

     this.comment=this.addComment.controls.comment.value;
    this.onCancelComment();

    var comment1={
      comment:this.comment
    }
    this.ticketService.addComment(this.ticketId,comment1).subscribe((res) => {
          var userDetailsL: any = res;
    })
    this.ticketService.getComment(this.ticketId).subscribe(data=>{
      this.getComment = data;  
       
    })

    

  }
  onCancelComment(){
    
    this.editComment=false
    this.commentStatus=false;
    this.addComment.reset();
    // this.updateComments.reset();

  }
  editComments(ids){
    this.editComment=ids._id;
    this.uid=ids._id;

  }
  onsubmitUpadteComment(){
    this.submitted=true;
    if (this.addComment.invalid) {
          return;
    }
    this.comment=this.addComment.controls.comment.value;
     
    // console.log(this.comment)
    var comment1={
      comment:this.comment
    }
    this.ticketService.updateComment(this.uid,comment1).subscribe((res) => {
          var userDetailsL: any = res;
          this.onCancelComment();
          this.toaster.open({
            text: "Success!! Created ",
            caption: "Sucess"+ ' notification',
            type: "success",
            position:"top-right",
            duration:900,
      
          });
         

    },
    catchError => {
    this.onCancelComment();

        if (catchError.status = 404) {
         this.toaster.open({
           text: "warning!! Updation "+catchError.error.message,
           caption: "Warning"+ ' notification',
           type: "warning",
           position:"top-right",
     
         });
     
        }
      }
    )

    this.ticketService.getComment(this.ticketId).subscribe(data=>{
      this.getComment = data;  
       
    })

  }
  onDeleteComment(commentid){
    this.uid=commentid._id;
    this.ticketService.deleteComment(this.uid).subscribe((res) => {
      var userDetailsL: any = res;
      // this.onCancelComment();
      this.toaster.open({
        text: "Success!! Created ",
        caption: "Sucess"+ ' notification',
        type: "success",
        position:"top-right",
        duration:900,
  
      });
      this.ticketService.getComment(this.ticketId).subscribe(data=>{
        this.getComment = data;  
         
      })

      this.router.navigate(['/pages/ticket/viewTicket'])
       },
       
       catchError => {
        if (catchError.status = 404) {
         this.toaster.open({
           text: "warning!! Deletion "+catchError.error.message,
           caption: "Warning"+ ' notification',
           type: "warning",
           position:"top-right",
     
         });
     
        }
      }
    
       )
       
      }


// ---------------------------------------Comment end----------------------------------------------------------------------------------

}
