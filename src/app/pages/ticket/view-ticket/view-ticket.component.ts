import { Component, OnInit } from '@angular/core';
// import { Gallery, ImageItem, GalleryItem } from '@ngx-gallery/core';
import { NbCalendarRange, NbDateService } from '@nebular/theme';
import { TicketService } from '../ticket.service';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { Toaster } from 'ngx-toast-notifications';
import { EmployeeService } from '../../employee/add-employee/employee.service';

@Component({
  selector: 'ngx-view-ticket',
  templateUrl: './view-ticket.component.html',
  styleUrls: ['./view-ticket.component.scss']
})
export class ViewTicketComponent implements OnInit {
  url:any ='';
  imageCount1;
  id=[];
  uid;
  ticketId;
  ticketIdm;
details;
detailsIssueType;
  image=[];
   assignto;
  getemployee=[];
  project;
  acessid=[];
  AccessForm;
  reporter;
  commentStatus=false;
  view=false;
  editComment=false;
  comment;
  updateDes=false;

  imageurl;
  detailsStatus;
  priority;
  issuetype;
  getComment;
  submitted;
  changeAssign:FormGroup;
  addComment: FormGroup;
  updatePri:FormGroup;
  updateStatus:FormGroup;
 datepick:FormGroup;
 imagesUrl;
  myDateValue: Date;
description;
name;
   date = new Date();
   ngModelDate =Date();
    imageObject:Array<object>= [];
  imageStrings=[];
  strings;
   teamleader=false;
   commentuser;
  
      constructor( 
                  private ticketService:TicketService,
                  private fb: FormBuilder,
                  private fb1: FormBuilder,
                  private updateSta: FormBuilder,
                  private updateEmp: FormBuilder,
                  private employeeService:EmployeeService,

                  protected dateService: NbDateService<Date>,

                  private router:Router,
                  private toaster:Toaster) {
                           
                
      }
    
      ngOnInit() {
        this.employeeService.getUser().subscribe(results=>{
          var userDetailsL:any =  results;
          this.commentuser=userDetailsL.user._id
          if(userDetailsL.user.role==2){
            this.teamleader=true;
          }
      

        })
        // console.log(this.id)

      

        //  this.myDateValue = new Date();
 
        this.addComment = this.fb.group({
          comment: [null, Validators.required],
        });
        this.updatePri=this.fb1.group({
          changePriority:['']
        });
        this.updateStatus=this.updateSta.group({
          changeStatus:['']
        });
        this.changeAssign=this.updateEmp.group({
          UpdateEmployee:['']
        });
        
        

        this.acessid=this.ticketService.getAccess();
      this.AccessForm=this.acessid[this.acessid.length-1];
       // console.log(this.AccessForm)
       if(this.AccessForm==1)
       {
         
        this.toaster.open({ 
            text:"This Page is only for View  Only",
            caption: "Access"+ ' notification',
            type: "danger",
            position:"top-right",
            // duration:1500
          });
        
     

       }

        // ---------------------------------------------------------
        this.id =this.ticketService.getService();
        // this.ticketId=this.id[this.id.length-1]._id
        localStorage.setItem('ticketIds',this.id[this.id.length-1]._id)
        
        this.ticketId=localStorage.getItem('ticketIds')
        

          this.ticketService.Ticketdetailsbyid(this.ticketId).subscribe((data:any)=>{
          this.details=data.data
        // this.ticketId=details._id
        this.name= this.details.name

        this.project= this.details.projectname._id
        this.assignto= this.details.assignto.name
        this.reporter= this.details.createdby.name
        this.image= this.details.duedate
        this.detailsStatus=this.details.status
        this.detailsIssueType=this.details.issuetype
        this.description=this.details.description
        // console.log(this.project)
        // console.log(this.assignto)
        // console.log(this.reporter)
        // console.log(this.image)
      
           

        // console.log(this.id)

        // ---------------------------------------------------------

         if( this.details.priority==1){
           this.priority="Higher";
         }
        if( this.details.priority==2){
          this.priority="High";
         }
          if( this.details.priority==3){
          this.priority="Medium";
         }
        if( this.details.priority==4){
          this.priority="Low";
        }
      
        // ---------------------------------------------------------
         this.ticketService.projectEmployeeList(this.project).subscribe((data: any) => {
          this.getemployee = data.employees;  
        });
      })

        // this.items = this.imageData.map(item => new ImageItem({src: item.srcUrl,thumb: item.previewUrl}));
        this.ticketService.getComment(this.ticketId).subscribe(data=>{
          this.getComment = data;  
           
        })

        this.ticketService.getImage(this.ticketId).subscribe((data:any)=>{
          this.imageStrings = data.data;  
            //  console.log(this.imageStrings[0].link)
            //  console.log("data.count")

            this.imageCount1=data.count;
            //  for(i=0;i<data.count;i++){
            //   console.log(i)
              
              this.imageStrings.forEach(x => {
                // console.log(x)
                var obj = {
                  image: x.link,
                  thumbImage: x.link,
                  
                }
                this.imageObject.push(obj)
              })
        
        })
       
         
        // console.log(this.AccessForm)

    }
    
          get f() { return this.addComment.controls; }
          get changePri() { return this.updatePri.controls; }
          get UpdateStatus1() { return this.updateStatus.controls; }
          get UpdateEmploye() { return this.changeAssign.controls; }

// ---------------------------------------Comment----------------------------------------------------------------------------------
          onComment(){
            this.commentStatus=true;

          }
          onsubmitComment(){
            this.submitted=true;
            if (this.addComment.invalid) {
                  return;
              }
      
             this.comment=this.addComment.controls.comment.value;
            this.onCancelComment();

            var comment1={
              comment:this.comment
            }
            this.ticketService.addComment(this.ticketId,comment1).subscribe((res) => {
                  var userDetailsL: any = res;
                  this.ticketService.getComment(this.ticketId).subscribe(data=>{
                    this.getComment = data;  
                     
                  })
             
            })
           
            

          }
          onCancelComment(){
            
            this.editComment=false
            this.commentStatus=false;
            this.addComment.reset();
            // this.updateComments.reset();

          }
          editComments(ids){
            this.editComment=ids._id;
            this.uid=ids._id;

          }
          onsubmitUpadteComment(){
            this.submitted=true;
            if (this.addComment.invalid) {
                  return;
            }
            this.comment=this.addComment.controls.comment.value;
             
            // console.log(this.comment)
            var comment1={
              comment:this.comment
            }
            this.ticketService.updateComment(this.uid,comment1).subscribe((res) => {
                  var userDetailsL: any = res;
                  this.onCancelComment();
                  this.toaster.open({
                    text: "Success!! Created ",
                    caption: "Sucess"+ ' notification',
                    type: "success",
                    position:"top-right",
                    duration:900,
              
                  });

            this.ticketService.getComment(this.ticketId).subscribe(data=>{
              this.getComment = data;  
               
            })

                 

            },
            catchError => {
            this.onCancelComment();

                if (catchError.status = 404) {
                 this.toaster.open({
                   text: "warning!! Updation "+catchError.error.message,
                   caption: "Warning"+ ' notification',
                   type: "warning",
                   position:"top-right",
             
                 });
             
                }
              }
            )
    
          }
          onDeleteComment(commentid){
            this.uid=commentid._id;
            this.ticketService.deleteComment(this.uid).subscribe((res) => {
              var userDetailsL: any = res;
              // this.onCancelComment();
              this.toaster.open({
                text: "Success!! Created ",
                caption: "Sucess"+ ' notification',
                type: "success",
                position:"top-right",
                duration:900,
          
              });
              this.ticketService.getComment(this.ticketId).subscribe(data=>{
                this.getComment = data;  
                 
              })
      
              this.router.navigate(['/pages/ticket/viewTicket'])
               },
               
               catchError => {
                if (catchError.status = 404) {
                 this.toaster.open({
                   text: "warning!! Deletion "+catchError.error.message,
                   caption: "Warning"+ ' notification',
                   type: "warning",
                   position:"top-right",
             
                 });
             
                }
              }
            
               )
               
              }
        
        
// ---------------------------------------Comment end----------------------------------------------------------------------------------
// ---------------------------------------Updaet Due date----------------------------------------------------------------------------------

// onDateChange(newDate: Date) {
//   console.log(newDate);
// }

Save(selectedDate){

// console.log(selectedDate)
var due={
  duedate:selectedDate
}
this.ticketService.updateDuedate(this.ticketId,due).subscribe((res) => {
  this.toaster.open({
    text: "Success!! Created ",
    caption: "Sucess"+ ' notification',
    type: "success",
    position:"top-right",
    duration:900,

  });

   
 },
 catchError=>{
       
  if(catchError.status = 404) {
    this.toaster.open({ 
      text: catchError.error.message,
      caption: "Warning"+ ' notification',
      type: "warning",
      position:"top-right",
            
    });
  }
}
)
}

// onGetDate(hai){
//   console.log(this.ngModelDate)
//   console.log(hai)
 
//   // console.log(this.datepick.controls.date.value)
//  }
// ---------------------------------------Updaet Priority end----------------------------------------------------------------------------------

 updatePriority(){
   var dbUpdatePriority=this.updatePri.controls.changePriority.value;
    var priority={
     priority:dbUpdatePriority
   }
   this.ticketService.updatePriority(this.ticketId,priority).subscribe((res) => {

   })

 }
// ---------------------------------------update Status  ----------------------------------------------------------------------------------
UpdateStatus(){
  var dbUpdateStatus=this.updateStatus.controls.changeStatus.value;
   var status={
    status:dbUpdateStatus
  }
  this.ticketService.UpdateStatus(this.ticketId,status).subscribe((res) => {

  })

}

// ---------------------------------------update employee  ----------------------------------------------------------------------------------
Updateemployee(){
  var dbUpdateEmp=this.changeAssign.controls.UpdateEmployee.value;
   var assignto={
    assignto:dbUpdateEmp
  }
  this.ticketService.updateEmployee(this.ticketId,assignto).subscribe((res) => {
  },
    
    catchError=>{
       
      if(catchError.status = 404) {
        this.toaster.open({ 
          text: catchError.error.message,
          caption: "Warning"+ ' notification',
          type: "warning",
          position:"top-right",
                
        });
      }
    }
  )


}


// ---------------------------------------Image update and delete  ----------------------------------------------------------------------------------


onSelectFile(event) {
  if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();

    reader.readAsDataURL(event.target.files[0]); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.url = event.target;
      this.imageurl=this.url.result
      var images={
        image:this.imageurl
      }
    
       
      this.ticketService.addImage(this.ticketId,images).subscribe((res) => {
        var result= res;
        // console.log(result)
    // this.imageObject=[];

        this.ticketService.getImage(this.ticketId).subscribe((data:any)=>{
          this.imageStrings = data.data;  
            //  console.log(this.imageStrings[0].link)
            //  console.log(data.count)
      
             this. imageCount1=data.count;
            //  for(i=0;i<data.count;i++){
            //   console.log(i)
              
              // this.imageStrings.forEach(x => {
              //   console.log(x)
                var obj = {
                  image:this.imageurl,
                  thumbImage:this.imageurl,
                  
                }
                this.imageObject.push(obj)
                // console.log(this.imageObject)
              // })   
        })
       
       
      })
      
     
      
    }
  }
  
}

deleteImage(imgid){
  this.imageObject=[];
  var imageid=imgid._id;
  // console.log(imageid)
  this.ticketService.deleteImage(imageid).subscribe((res:any)=>{
    this.ticketService.getImage(this.ticketId).subscribe((data:any)=>{
      this.imageStrings = data.data;  
        //  console.log(this.imageStrings[0].link)
        //  console.log(data.count)
        // console.log(this.imageObject.length)
        if(this.imageObject.length==0){
          this.onCloseDeleteModal()
        }
        else{
         var imageCount=data.count;
             var obj = {
              image: this.imageStrings[imageCount-1].link,
              thumbImage: this.imageStrings[imageCount-1].link,
              
            }
            this.imageObject.push(obj)
          }
       })
  },
    catchError=>{
       
      if(catchError.status = 404) {
        this.toaster.open({ 
          text: catchError.error.message,
          caption: "Warning"+ ' notification',
          type: "warning",
          position:"top-right",
                
        });
      }
    }
  )
   

  
}
onCloseDeleteModal(){
  this.imageObject=[];
  this.ticketService.getImage(this.ticketId).subscribe((data:any)=>{
    this.imageStrings = data.data;  
      //  console.log(this.imageStrings[0].link)
      //  console.log(data.count)

       this.imageCount1=data.count;
      //  for(i=0;i<data.count;i++){
      //   console.log(i)
        
        this.imageStrings.forEach(x => {
          // console.log(x)
          var obj = {
            image: x.link,
            thumbImage:x.link,
            
          }
          this.imageObject.push(obj)
        })   
  })

}
// ---------------------------------------Image update and delete end  ----------------------------------------------------------------------------------
// ---------------------------------------update description  ----------------------------------------------------------------------------------

triggerUpdateDescription(){
  this.updateDes=true;
}
onsubmitDescription(){
  this.submitted=true;
  if (this.addComment.invalid) {
        return;
    }

   var description=this.addComment.controls.comment.value;
  this.onCancelDescription();

  var description1={
    description:description
  }
  this.ticketService.updateDescription(this.ticketId,description1).subscribe((res) => {
        var userDetailsL: any = res;
        this.ticketService.Ticketdetailsbyid(this.ticketId).subscribe((data:any)=>{
          this.details=data.data
        // this.ticketId=details._id
        this.project= this.details.projectname._id
        this.assignto= this.details.assignto.name
        this.reporter= this.details.createdby.name
        this.image= this.details.duedate
        this.description=this.details.description

        })

  },
  catchError=>{
       
    if(catchError.status = 404) {
      this.toaster.open({ 
        text: catchError.error.message,
        caption: "Warning"+ ' notification',
        type: "warning",
        position:"top-right",
              
      });
    }
  })
  
}
onCancelDescription(){
            
  // this.editComment=false
  this.updateDes=false;
  this.addComment.reset();
  // this.updateComments.reset();

}



}
     




