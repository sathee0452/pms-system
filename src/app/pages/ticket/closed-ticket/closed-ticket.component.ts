import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { LoginService } from '../../../login/login-service.service';

@Component({
  selector: 'ngx-closed-ticket',
  templateUrl: './closed-ticket.component.html',
  styleUrls: ['./closed-ticket.component.scss']
})
export class ClosedTicketComponent implements OnInit {
closedTicket=[];
assignedTickets=[];
getDescription;
searchName;
access=4;

count1=0;
 users=false;
  constructor(private router:Router,
    private formBuilder:FormBuilder,
    private ticketService:TicketService,
    private loginService:LoginService) { }

  ngOnInit() {
    this.ticketService.setAccess(this.access);

    // --------------------------------
    this.loginService.getUser().subscribe(results => {
      var userDetailsL: any = results;
      //alert("Successs"+userDetailsL.user.name+userDetailsL.user.role)
      if (userDetailsL.user.role == 1) {
        this.users = true;
      }
 
    });

    //  ---------------created by ticket-------------------------------------
  this.ticketService.getTicketByYou()  
  .subscribe((data:any) => {  
    this.closedTicket = data; 
    for(let i of data) {
      if(i.status=="closed"){
       this.count1=this.count1+1;
      }
    }
    // console.log(this.count1)

  });
   
  }
  getDes(tick){
    this.getDescription=tick.description;
  }
  Search(value) {
    var searchvalue={
          name:value
    }
    this.ticketService.searchTicket(searchvalue).subscribe((data: any) => {
      this.closedTicket = data;
    });
  }
  closedForYou(){
    this.count1=0;
    //  ---------------Assugned for You-------------------------------------
   this.ticketService.getTicketForYou()  
   .subscribe((data:any) => {  
     this.closedTicket = data; 
     for(let i of data) {
       if(i.status=="closed"){
        this.count1=this.count1+1;
       }
     }
    //  console.log(this.count1)

   });



  }
  viewDetails(id){
    this.ticketService.setService(id);
    this.router.navigate(['/pages/ticket/viewTicket'])
  }
  closedByYou(){
    this.count1=0;
    //  ---------------created by ticket-------------------------------------
  this.ticketService.getTicketByYou()  
  .subscribe((data:any) => {  
    this.closedTicket = data;  

    for(let i of data) {
      if(i.status=="closed"){
       this.count1=this.count1+1;
      }
    }
    // console.log(this.count1)

  });

   

  }
  }


