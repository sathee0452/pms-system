import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TicketService } from '../ticket.service';
import { NbDialogService } from '@nebular/theme';
import { CookieService } from 'ngx-cookie-service';
import { Toast, Toaster } from 'ngx-toast-notifications';
//import { ShowcaseDialogComponent } from './showcase-dialog/showcase-dialog.component';


@Component({
  selector: 'ngx-created-by-you',
  templateUrl: './created-by-you.component.html',
  styleUrls: ['./created-by-you.component.scss']
})
export class CreatedByYouComponent implements OnInit {
  sortStatus:FormGroup
  createdbyyou=[];
status="closed";
selectedticket=[];
selectedticketid;
getDescription=[];
searchName;
access=3;
search;
sino=[];
size=10;
paginationAccess=1;

//status:FormGroup
 pageno: number;
perPage: number;
total: number;
totalPages: number;




// tick=[];
  constructor(private router:Router,
    private formBuilder:FormBuilder,
    private ticketService:TicketService,
    private dialogService: NbDialogService,
    private cookie:CookieService,
    private toaster:Toaster) { 
      this.pageno=1;
    }

  ngOnInit() {
    this.getUsuarios(this.pageno);

    this.ticketService.setAccess(this.access);

      //  ---------------get  all ticket created by you-------------------------------------
      this.sortStatus=this.formBuilder.group({
        sort:['']
      });
    

  }

  getUsuarios(pageno1) {
    this.paginationAccess=1;
    this.sino=[];
     var tick={
       pageno : pageno1,
       size:10
       }
       
     this.ticketService.getTicketByYoupagination(tick).subscribe((data:any) => {
         this.createdbyyou = data.data;
         this.pageno = data.current;
         this.total = data.pages;
         this.totalPages =data.pages;
         });
         for(var i=0;i<this.size;i++){
           var counts=(this.size*(pageno1-1))+(i+1);
           this.sino.push(counts)
         }
         // console.log(this.sino)
      
           }
     mudouPagina(evento) {
       this.getUsuarios(evento.valor); 
     }
     mudouPagina2(evento) {
      this.statusPagination(evento.valor); 
    }
  conform(tick){
    this.selectedticket = tick.name;
    this.selectedticketid=tick._id;
  }

  getDes(tick){
    this.getDescription=tick.description;
  }

  //--------------------------change ticket status----------------------------------------------
 
  onChangeStatus(){
    //var id=[this.selectedticketid],
    var change={
       status:this.status,
    }
    
    this.ticketService.changeStatus(this.selectedticketid,change).subscribe((res) => {
      var userDetailsL: any = res;
      //this.router.navigate(['/nav'])
      this.toaster.open({
        text: " Ticket Closed Successfully :)",
        caption: "Success"+ ' notification',
        type: "success",
        position:"top-right",
        duration:1500,
  
      });
      this.router.navigate(['/pages/ticket/closedTicket']) 
    }
    )
 
 
    catchError => {
      if (catchError.status = 404) {
        alert(catchError.error.message)
      }
    }
 
 
  }
  onReset() {
    //this.submitted = false;
   // this.add_ticket.reset();
 }
 onSortStatus(){
  this.paginationAccess=2;
  if(this.sortStatus.controls.sort.value==="all"){
    var pageStatus =1;

    this.getUsuarios(pageStatus)
  }
  else{
    var pageStatus =1;
    this.statusPagination(pageStatus)

     
  }   
  }

  statusPagination(page){
    this.sino=[];

    var sort={
      status:this.sortStatus.controls.sort.value,
      pageno:page,
      size:this.size
    }
    // console.log(page)
    this.ticketService.Sortstatusbyyou(sort).subscribe((res:any) => {
      this.createdbyyou=res.data;
      this.pageno = res.current;
      this.total = res.pages;
      this.totalPages =res.pages;
      
      for(var i=0;i<this.size;i++){
        var counts=(this.size*(page-1))+(i+1);
        this.sino.push(counts)
      }
    });

  }
  

 viewDetails(id){
  this.ticketService.setService(id);
  this.router.navigate(['/pages/ticket/viewTicket'])
}
Search(value) {
  var searchvalue={
        name:value
  }
  this.ticketService.Searchticketbyyou(searchvalue).subscribe((data: any) => {
    this.createdbyyou = data;
  });
}

}


 