import { NgModule } from '@angular/core';
 
import { FormsModule as ngFormsModule ,ReactiveFormsModule } from '@angular/forms';
import {
   
  NbButtonModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbTabsetModule, 
   NbInputModule,
    NbSelectModule,
     NbActionsModule,
       NbTooltipModule, 
       NbIconModule,
       NbLayoutModule,
       NbDatepickerModule,
       NbUserModule,
       NbRadioModule,
       NbCheckboxComponent,
       NbCheckboxModule,
} from '@nebular/theme';
 
 

 

import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';

 
import { ThemeModule } from '../../@theme/theme.module';
import { TicketRoutingModule } from './ticket-routing.module';
import { TicketComponent } from './ticket.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';
import { CreatedByYouComponent } from './created-by-you/created-by-you.component';
import { AssignForYouComponent } from './assign-for-you/assign-for-you.component';
import { ClosedTicketComponent } from './closed-ticket/closed-ticket.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ViewTicketComponent } from './view-ticket/view-ticket.component';
 
import { NgImageSliderModule } from 'ng-image-slider';
 import { TicketCommentComponent } from './view-ticket/ticket-comment/ticket-comment.component';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  imports: [
    ngFormsModule,
    NbInputModule,
     ReactiveFormsModule,
    ThemeModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    
    NbCardModule,
    NbButtonModule,
    TicketRoutingModule,
    NbSelectModule,    
    NbActionsModule,
    NbTooltipModule,
    NbIconModule,
    NgxPaginationModule,
    NbRadioModule,
    
     
    
    DatepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),

    NbUserModule,
    NbCheckboxModule,
  NbUserModule,
    NgImageSliderModule
    
     
    
  ],
  declarations: [
    TicketComponent,
     
     TicketListComponent,
     AddTicketComponent,
     CreatedByYouComponent,
     AssignForYouComponent,
     ClosedTicketComponent,
     ViewTicketComponent,
     PaginationComponent,
     TicketCommentComponent

      
     
  ],
  providers: [ ],
  bootstrap:[ TicketComponent ]

})
export class TicketModule { }
