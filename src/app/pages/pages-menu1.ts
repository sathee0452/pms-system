import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS1: any[] = [
  {
    title: 'Home',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
   {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'Ticket',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'List of Tickets',
        link: '/pages/ticket/listTicket',
        roles:[1,2,3]
      },
      {
        title: 'Create Ticket',
        link: '/pages/ticket/addticket',
      },
     {
        title: 'Created By You',
        link: '/pages/ticket/createdByYou',
      },
      {
        title: 'Assigned For You',
        link: '/pages/ticket/assignedForYou',
      },
       
      {
        title: 'Closed Ticket',
        link: '/pages/ticket/closedTicket',
      },
  
      
       
    ],
  },
  {
    title: 'employee',
    icon: 'people-outline',
    children: [
      {
        title: 'List of Employee',
        link: '/pages/employee/employee-list',
      },
    
      // {
      //   title: 'Add Employee',
      //   link: '/pages/employee/add-employee',
      // },
       ],
  },

  {
    title: 'project',
    icon: 'email-outline',
    children: [
      {
        title: 'List of Project',
        link: '/pages/project/project-list',
      },
      {
        title: 'Add project',
        link: '/pages/project/add-project',
      },
       ],
  },
  
    
   ];
