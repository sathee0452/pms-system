import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_helper/auth.guard';
import { AuthGuardPage } from './_helper/auth.guard copy';
 

import { RequestComponent } from './request/request.component';

const routes: Routes = [
   {

    path: 'pages',loadChildren: () => import('../app/pages/pages.module')
      .then(m => m.PagesModule),canActivate:[AuthGuard]
  },
  {path:'',component:LoginComponent,canActivate:[AuthGuardPage]},
  {path:'forgotPassword',component:RequestComponent},
  
  
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: '**', redirectTo: 'pages' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
