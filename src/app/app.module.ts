/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import { ReactiveFormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CookieService } from 'ngx-cookie-service';
import { ToastNotificationsModule } from "ngx-toast-notifications";


import {
   NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
   NbActionsModule,
  NbButtonModule,
  NbAlertModule,
  NbDatepickerModule,
  NbInputModule,
  NbCardComponent,
  NbCardModule,
} from '@nebular/theme';
 

import { LoginComponent } from './login/login.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
 
import { RequestComponent } from './request/request.component';
import { AuthComponent } from './auth/auth.component';
  
 
 
 

 
@NgModule({
  declarations: [AppComponent,LoginComponent, RequestComponent, AuthComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbActionsModule,
    ReactiveFormsModule,
    NbAlertModule,
    NbButtonModule,  
   // NgxEditorModule,  
    ThemeModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(), 
    NbDialogModule.forRoot(),
     NbDatepickerModule.forRoot(),
     NbInputModule,
      NbCardModule,

 

    
    ToastNotificationsModule
  ],
  providers:[CookieService,{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent],
})
export class AppModule {
}
