import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

//import { AuthenticationService } from '../_service/authentication.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({ providedIn: 'root' })
export class AuthGuardPage implements CanActivate {
    constructor(
        private router: Router,
        private cookies:CookieService,
        //private authenticationService: AuthenticationService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
       var currentUser =localStorage.getItem('user');
        if (!currentUser) {
            // authorised so return true if ()
            return true
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/pages/dashboard']);
        return false;
    }
    canLoad(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        var currentUser = this.cookies.get('userAuth');
        if(currentUser) {
            this.router.navigate(['/pages/dashboard']);
        }
    }
}