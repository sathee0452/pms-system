import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

 import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LoginService } from '../../../login/login-service.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],   
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
 display=[];
 role=[];
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

   
  currentTheme = 'default';

  userMenu = [ { title: 'Profile' }, { title: 'Log out' } ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private loginService: LoginService,
              private cookie:CookieService,
              private router:Router,

              private breakpointService: NbMediaBreakpointsService) {
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;
    this.loginService.getUser().subscribe(results=>{
        var userDetailsL:any =  results;
        if(userDetailsL.user.role){
           this.display=userDetailsL.user.name;
           this.role=userDetailsL.user.role;

        }
      });
     
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

   
  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
     

    return false;
  }
  userProfile(){
    this.router.navigate(['/pages/employee/user-profile'])

  }
  navigateHome() {
    this.router.navigate(['/pages/dashboard'])

    this.menuService.navigateHome();
    return false;

  }


  logout(){
    //this.display=true;
    localStorage.removeItem('user')
    this.router.navigate(['/'])
    }
}
