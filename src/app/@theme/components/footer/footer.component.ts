import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
  <div class="test">
    <span class="created-by">Created with ♥ by <b><a href="http://www.jadfocus.com" target="_blank">JadFocus</a></b> @ 2020</span>
       </div>
  `,
})
export class FooterComponent {
}
