/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  baseUrl:"http://35.244.57.252:4000/api"
  //  baseUrl:"http://192.168.43.113:4000/api"
};
 